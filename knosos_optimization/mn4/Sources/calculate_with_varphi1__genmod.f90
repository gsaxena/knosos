        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:50:59 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALCULATE_WITH_VARPHI1__genmod
          INTERFACE 
            SUBROUTINE CALCULATE_WITH_VARPHI1(IB,REGB,PHI1C,GRHS,QRHS,  &
     &L1RHS,L2RHS,L3RHS,GPHI,QPHI,L1PHI,L2PHI,L3PHI,GB,QB,L1B,L2B,L3B,  &
     &N1NMRHS,MBBNMRHS,TRMNMRHS,N1NMB,MBBNM,TRMNM)
              USE GLOBAL
              INTEGER(KIND=4) :: IB
              INTEGER(KIND=4) :: REGB
              REAL(KIND=8) :: PHI1C(NNMP)
              REAL(KIND=8) :: GRHS(NNMP,NNMP)
              REAL(KIND=8) :: QRHS(NNMP,NNMP)
              REAL(KIND=8) :: L1RHS(NNMP,NNMP)
              REAL(KIND=8) :: L2RHS(NNMP,NNMP)
              REAL(KIND=8) :: L3RHS(NNMP,NNMP)
              REAL(KIND=8) :: GPHI(NNMP)
              REAL(KIND=8) :: QPHI(NNMP)
              REAL(KIND=8) :: L1PHI(NNMP)
              REAL(KIND=8) :: L2PHI(NNMP)
              REAL(KIND=8) :: L3PHI(NNMP)
              REAL(KIND=8) :: GB
              REAL(KIND=8) :: QB
              REAL(KIND=8) :: L1B
              REAL(KIND=8) :: L2B
              REAL(KIND=8) :: L3B
              REAL(KIND=8) :: N1NMRHS(NNMP,NNMP)
              REAL(KIND=8) :: MBBNMRHS(NNMP,NNMP)
              REAL(KIND=8) :: TRMNMRHS(NNMP,NNMP)
              REAL(KIND=8) :: N1NMB(NNMP)
              REAL(KIND=8) :: MBBNM(NNMP)
              REAL(KIND=8) :: TRMNM(NNMP)
            END SUBROUTINE CALCULATE_WITH_VARPHI1
          END INTERFACE 
        END MODULE CALCULATE_WITH_VARPHI1__genmod
