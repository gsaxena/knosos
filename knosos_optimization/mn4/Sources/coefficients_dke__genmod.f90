        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COEFFICIENTS_DKE__genmod
          INTERFACE 
            SUBROUTINE COEFFICIENTS_DKE(NPOINT,I_W,I_L,NW,Z1,T1,B1,HBPP1&
     &,VD1,ZB,TB,BB,HBPPB,VDB,Z2,T2,B2,HBPP2,VD2,NLAMBDA,LAMBDA,ZLW,TLW,&
     &ZRW,TRW,BI1,BI2,BI3,BI4,BI5,BI6,BI7,NMODES,BI8)
              INTEGER(KIND=4) :: NMODES
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NW
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: I_W(NPOINT)
              INTEGER(KIND=4) :: I_L(NPOINT)
              REAL(KIND=8) :: Z1(NW)
              REAL(KIND=8) :: T1(NW)
              REAL(KIND=8) :: B1(NW)
              REAL(KIND=8) :: HBPP1(NW)
              REAL(KIND=8) :: VD1(3,NW)
              REAL(KIND=8) :: ZB(NW)
              REAL(KIND=8) :: TB(NW)
              REAL(KIND=8) :: BB(NW)
              REAL(KIND=8) :: HBPPB(NW)
              REAL(KIND=8) :: VDB(3,NW)
              REAL(KIND=8) :: Z2(NW)
              REAL(KIND=8) :: T2(NW)
              REAL(KIND=8) :: B2(NW)
              REAL(KIND=8) :: HBPP2(NW)
              REAL(KIND=8) :: VD2(3,NW)
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              REAL(KIND=8) :: ZLW(NPOINT)
              REAL(KIND=8) :: TLW(NPOINT)
              REAL(KIND=8) :: ZRW(NPOINT)
              REAL(KIND=8) :: TRW(NPOINT)
              REAL(KIND=8) :: BI1(NPOINT)
              REAL(KIND=8) :: BI2(NPOINT)
              REAL(KIND=8) :: BI3(NPOINT)
              REAL(KIND=8) :: BI4(NPOINT)
              REAL(KIND=8) :: BI5(NPOINT)
              REAL(KIND=8) :: BI6(NPOINT)
              REAL(KIND=8) :: BI7(NPOINT)
              REAL(KIND=8) :: BI8(NPOINT,NMODES)
            END SUBROUTINE COEFFICIENTS_DKE
          END INTERFACE 
        END MODULE COEFFICIENTS_DKE__genmod
