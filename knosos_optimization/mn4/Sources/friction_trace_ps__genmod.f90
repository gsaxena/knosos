        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:46 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FRICTION_TRACE_PS__genmod
          INTERFACE 
            SUBROUTINE FRICTION_TRACE_PS(JT,IB,NBB,ZB,AB,NB,DNBDPSI,TB, &
     &DTBDPSI,EPSI,F_C,F_S,NALPHAB,ZETA,THETA,B,EXPMZEPOT,U0,U1,U2,GB)
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: F_C
              REAL(KIND=8) :: F_S
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: EXPMZEPOT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: U0(NALPHAB,NALPHAB)
              REAL(KIND=8) :: U1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: U2(NALPHAB,NALPHAB)
              REAL(KIND=8) :: GB
            END SUBROUTINE FRICTION_TRACE_PS
          END INTERFACE 
        END MODULE FRICTION_TRACE_PS__genmod
