        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:43 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE DKE_CONSTANTS__genmod
          INTERFACE 
            SUBROUTINE DKE_CONSTANTS(IB,NBB,ZB,AB,REGB,NB,DNBDPSI,TB,   &
     &DTBDPSI,EPSI,FLAG)
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              INTEGER(KIND=4) :: REGB(NBB)
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              LOGICAL(KIND=4) :: FLAG
            END SUBROUTINE DKE_CONSTANTS
          END INTERFACE 
        END MODULE DKE_CONSTANTS__genmod
