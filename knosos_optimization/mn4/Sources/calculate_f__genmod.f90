        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:46 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALCULATE_F__genmod
          INTERFACE 
            SUBROUTINE CALCULATE_F(NALPHAB,ZETA,THETA,B,F_C,F_S)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: F_C
              REAL(KIND=8) :: F_S
            END SUBROUTINE CALCULATE_F
          END INTERFACE 
        END MODULE CALCULATE_F__genmod
