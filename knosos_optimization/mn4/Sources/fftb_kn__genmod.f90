        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FFTB_KN__genmod
          INTERFACE 
            SUBROUTINE FFTB_KN(NALPHAB,QNM,Q)
              INTEGER(KIND=4) :: NALPHAB
              COMPLEX(KIND=8) :: QNM(NALPHAB,NALPHAB)
              REAL(KIND=8) :: Q(NALPHAB,NALPHAB)
            END SUBROUTINE FFTB_KN
          END INTERFACE 
        END MODULE FFTB_KN__genmod
