        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_ORBI__genmod
          INTERFACE 
            SUBROUTINE FILL_ORBI(NUMZ,NUMT,QNMC_IN,NUM,QNM_OUT)
              USE GLOBAL
              INTEGER(KIND=4) :: NUM
              INTEGER(KIND=4) :: NUMT
              INTEGER(KIND=4) :: NUMZ
              COMPLEX(KIND=8) :: QNMC_IN(NUMZ,NUMT)
              REAL(KIND=8) :: QNM_OUT(NUM)
            END SUBROUTINE FILL_ORBI
          END INTERFACE 
        END MODULE FILL_ORBI__genmod
