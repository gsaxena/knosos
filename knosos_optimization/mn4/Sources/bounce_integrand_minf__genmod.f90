        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:08 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE BOUNCE_INTEGRAND_MINF__genmod
          INTERFACE 
            SUBROUTINE BOUNCE_INTEGRAND_MINF(IW,Z_L,ZB,LAMBDA,BB,BPB,   &
     &HBPPB,VDB,NQ,QINT)
              INTEGER(KIND=4) :: NQ
              INTEGER(KIND=4) :: IW
              REAL(KIND=8) :: Z_L
              REAL(KIND=8) :: ZB
              REAL(KIND=8) :: LAMBDA
              REAL(KIND=8) :: BB
              REAL(KIND=8) :: BPB
              REAL(KIND=8) :: HBPPB
              REAL(KIND=8) :: VDB(3)
              REAL(KIND=8) :: QINT(NQ)
            END SUBROUTINE BOUNCE_INTEGRAND_MINF
          END INTERFACE 
        END MODULE BOUNCE_INTEGRAND_MINF__genmod
