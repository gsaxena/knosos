        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FSA2__genmod
          INTERFACE 
            FUNCTION FSA2(NA,NZ,THETAP,FUNC,JAC,FDEGR)
              INTEGER(KIND=4) :: NZ
              INTEGER(KIND=4) :: NA
              REAL(KIND=8) :: THETAP(NA)
              REAL(KIND=8) :: FUNC(NA,NZ)
              REAL(KIND=8) :: JAC(NA,NZ)
              INTEGER(KIND=4) :: FDEGR
              REAL(KIND=8) :: FSA2
            END FUNCTION FSA2
          END INTERFACE 
        END MODULE FSA2__genmod
