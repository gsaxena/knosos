        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FSA__genmod
          INTERFACE 
            FUNCTION FSA(NZ,NT,FUNC,JAC,FDEGR)
              INTEGER(KIND=4) :: NT
              INTEGER(KIND=4) :: NZ
              REAL(KIND=8) :: FUNC(NZ,NT)
              REAL(KIND=8) :: JAC(NZ,NT)
              INTEGER(KIND=4) :: FDEGR
              REAL(KIND=8) :: FSA
            END FUNCTION FSA
          END INTERFACE 
        END MODULE FSA__genmod
