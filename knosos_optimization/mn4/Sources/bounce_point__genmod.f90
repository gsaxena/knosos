        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:08 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE BOUNCE_POINT__genmod
          INTERFACE 
            SUBROUTINE BOUNCE_POINT(Z_IN,T_IN,BBOUNCE,Z_LIM,T_LIM,Z_OUT,&
     &T_OUT,BP_OUT,HBPP_OUT,VD_OUT,FLAG)
              USE GLOBAL
              REAL(KIND=8) :: Z_IN
              REAL(KIND=8) :: T_IN
              REAL(KIND=8) :: BBOUNCE
              REAL(KIND=8) :: Z_LIM
              REAL(KIND=8) :: T_LIM
              REAL(KIND=8) :: Z_OUT
              REAL(KIND=8) :: T_OUT
              REAL(KIND=8) :: BP_OUT
              REAL(KIND=8) :: HBPP_OUT
              REAL(KIND=8) :: VD_OUT(3)
              INTEGER(KIND=4) :: FLAG
            END SUBROUTINE BOUNCE_POINT
          END INTERFACE 
        END MODULE BOUNCE_POINT__genmod
