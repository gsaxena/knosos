        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:46 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_TRACE1NU_O_LOWCOLL__genmod
          INTERFACE 
            SUBROUTINE CALC_TRACE1NU_O_LOWCOLL(JT,IB,NBB,ZB,AB,REGB,S,NB&
     &,DNBDPSI,TB,DTBDPSI,EPSI,PHI1C,MBBNM,TRMNM,GB)
              USE GLOBAL
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              INTEGER(KIND=4) :: REGB(NBB)
              REAL(KIND=8) :: S
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: PHI1C(NNMP)
              REAL(KIND=8) :: MBBNM(NNMP)
              REAL(KIND=8) :: TRMNM(NNMP)
              REAL(KIND=8) :: GB
            END SUBROUTINE CALC_TRACE1NU_O_LOWCOLL
          END INTERFACE 
        END MODULE CALC_TRACE1NU_O_LOWCOLL__genmod
