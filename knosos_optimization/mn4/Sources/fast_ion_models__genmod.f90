        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:19 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FAST_ION_MODELS__genmod
          INTERFACE 
            SUBROUTINE FAST_ION_MODELS(VS,IS,NS,NALPHA,NALPHAB,NLAMBDA, &
     &LAMBDA,I_P,NPOINT,BI1,BI3,BI4,BI6,ZLW,ZRW,THETAP,THETA,B_AL,VDS_AL&
     &,TAU,IA_OUT)
              USE GLOBAL
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              INTEGER(KIND=4) :: NS
              REAL(KIND=8) :: VS(NS)
              INTEGER(KIND=4) :: IS
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
              REAL(KIND=8) :: BI1(NPOINT)
              REAL(KIND=8) :: BI3(NPOINT)
              REAL(KIND=8) :: BI4(NPOINT)
              REAL(KIND=8) :: BI6(NPOINT)
              REAL(KIND=8) :: ZLW(NPOINT)
              REAL(KIND=8) :: ZRW(NPOINT)
              REAL(KIND=8) :: THETAP(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: VDS_AL(NNMP,NALPHA,NALPHAB)
              REAL(KIND=8) :: TAU(NPOINT)
              INTEGER(KIND=4) :: IA_OUT(NPOINT)
            END SUBROUTINE FAST_ION_MODELS
          END INTERFACE 
        END MODULE FAST_ION_MODELS__genmod
