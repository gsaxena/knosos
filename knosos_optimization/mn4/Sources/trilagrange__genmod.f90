        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE TRILAGRANGE__genmod
          INTERFACE 
            SUBROUTINE TRILAGRANGE(X1A,X2A,X3A,YA,M,N,O,X1,X2,X3,Y,ORDER&
     &)
              INTEGER(KIND=4) :: O
              INTEGER(KIND=4) :: N
              INTEGER(KIND=4) :: M
              REAL(KIND=8) :: X1A(M)
              REAL(KIND=8) :: X2A(N)
              REAL(KIND=8) :: X3A(O)
              REAL(KIND=8) :: YA(M,N,O)
              REAL(KIND=8) :: X1
              REAL(KIND=8) :: X2
              REAL(KIND=8) :: X3
              REAL(KIND=8) :: Y
              INTEGER(KIND=4) :: ORDER
            END SUBROUTINE TRILAGRANGE
          END INTERFACE 
        END MODULE TRILAGRANGE__genmod
