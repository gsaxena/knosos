        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CHECK_JACSIGN__genmod
          INTERFACE 
            SUBROUTINE CHECK_JACSIGN(NZ,NT,X1,X2,X3,BZT,LEFTHANDED)
              INTEGER(KIND=4) :: NT
              INTEGER(KIND=4) :: NZ
              REAL(KIND=8) :: X1(3,NZ,NT)
              REAL(KIND=8) :: X2(3,NZ,NT)
              REAL(KIND=8) :: X3(3,NZ,NT)
              REAL(KIND=8) :: BZT(NZ,NT)
              LOGICAL(KIND=4) :: LEFTHANDED
            END SUBROUTINE CHECK_JACSIGN
          END INTERFACE 
        END MODULE CHECK_JACSIGN__genmod
