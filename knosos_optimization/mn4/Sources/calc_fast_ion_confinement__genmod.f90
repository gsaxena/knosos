        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:19 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_FAST_ION_CONFINEMENT__genmod
          INTERFACE 
            SUBROUTINE CALC_FAST_ION_CONFINEMENT(S,IS,NS,NAL,NLAMBDA)
              USE GLOBAL
              INTEGER(KIND=4) :: NS
              REAL(KIND=8) :: S(NS)
              INTEGER(KIND=4) :: IS
              INTEGER(KIND=4) :: NAL
              INTEGER(KIND=4) :: NLAMBDA
            END SUBROUTINE CALC_FAST_ION_CONFINEMENT
          END INTERFACE 
        END MODULE CALC_FAST_ION_CONFINEMENT__genmod
