        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:19 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE SET_INITIAL_CONDITION__genmod
          INTERFACE 
            SUBROUTINE SET_INITIAL_CONDITION(S0,ZLW,TLW,ZRW,TRW,E_O_MU, &
     &BI1,BI3,BI4,BI6,T,S,ALPHA,THETA,ZETA,ZL,TL,ZR,TR,J,DJDS,DJDA,DSDT,&
     &DADT,DSDA)
              REAL(KIND=8) :: S0
              REAL(KIND=8) :: ZLW
              REAL(KIND=8) :: TLW
              REAL(KIND=8) :: ZRW
              REAL(KIND=8) :: TRW
              REAL(KIND=8) :: E_O_MU
              REAL(KIND=8) :: BI1
              REAL(KIND=8) :: BI3
              REAL(KIND=8) :: BI4
              REAL(KIND=8) :: BI6
              REAL(KIND=8) :: T
              REAL(KIND=8) :: S
              REAL(KIND=8) :: ALPHA
              REAL(KIND=8) :: THETA
              REAL(KIND=8) :: ZETA
              REAL(KIND=8) :: ZL
              REAL(KIND=8) :: TL
              REAL(KIND=8) :: ZR
              REAL(KIND=8) :: TR
              REAL(KIND=8) :: J
              REAL(KIND=8) :: DJDS
              REAL(KIND=8) :: DJDA
              REAL(KIND=8) :: DSDT
              REAL(KIND=8) :: DADT
              REAL(KIND=8) :: DSDA
            END SUBROUTINE SET_INITIAL_CONDITION
          END INTERFACE 
        END MODULE SET_INITIAL_CONDITION__genmod
