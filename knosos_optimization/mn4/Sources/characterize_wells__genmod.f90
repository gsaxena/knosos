        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CHARACTERIZE_WELLS__genmod
          INTERFACE 
            SUBROUTINE CHARACTERIZE_WELLS(NAL,NA,NALPHA,NW,Z1,T1,B1,    &
     &HBPP1,VD1,ZB,TB,BB,HBPPB,VDB,Z2,T2,B2,HBPP2,VD2,BT,BTT,ALPHAP_W,  &
     &DALPHAP,BOTTOM,CONNECTED,OFFSET)
              INTEGER(KIND=4) :: NAL
              INTEGER(KIND=4) :: NA
              INTEGER(KIND=4) :: NALPHA
              INTEGER(KIND=4) :: NW
              REAL(KIND=8) :: Z1(5000)
              REAL(KIND=8) :: T1(5000)
              REAL(KIND=8) :: B1(5000)
              REAL(KIND=8) :: HBPP1(5000)
              REAL(KIND=8) :: VD1(3,5000)
              REAL(KIND=8) :: ZB(5000)
              REAL(KIND=8) :: TB(5000)
              REAL(KIND=8) :: BB(5000)
              REAL(KIND=8) :: HBPPB(5000)
              REAL(KIND=8) :: VDB(3,5000)
              REAL(KIND=8) :: Z2(5000)
              REAL(KIND=8) :: T2(5000)
              REAL(KIND=8) :: B2(5000)
              REAL(KIND=8) :: HBPP2(5000)
              REAL(KIND=8) :: VD2(3,5000)
              REAL(KIND=8) :: BT(5000)
              REAL(KIND=8) :: BTT(5000)
              REAL(KIND=8) :: ALPHAP_W(5000)
              REAL(KIND=8) :: DALPHAP
              LOGICAL(KIND=4) :: BOTTOM(5000)
              LOGICAL(KIND=4) :: CONNECTED(5000,5000)
              REAL(KIND=8) :: OFFSET
            END SUBROUTINE CHARACTERIZE_WELLS
          END INTERFACE 
        END MODULE CHARACTERIZE_WELLS__genmod
