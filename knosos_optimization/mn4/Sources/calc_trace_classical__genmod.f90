        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:46 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_TRACE_CLASSICAL__genmod
          INTERFACE 
            SUBROUTINE CALC_TRACE_CLASSICAL(JT,IB,NBB,ZB,AB,S,NB,DNBDPSI&
     &,TB,DTBDPSI,EPSI,NALPHAB,B,ABSNABLAPSI2OB2,EXPMZEPOT,GB)
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              REAL(KIND=8) :: S
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: ABSNABLAPSI2OB2(NALPHAB,NALPHAB)
              REAL(KIND=8) :: EXPMZEPOT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: GB
            END SUBROUTINE CALC_TRACE_CLASSICAL
          END INTERFACE 
        END MODULE CALC_TRACE_CLASSICAL__genmod
