        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:08 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE BOUNCE_INTEGRAND__genmod
          INTERFACE 
            SUBROUTINE BOUNCE_INTEGRAND(IW,Z_INI,Z_L,T_L,COSNM,SINNM,   &
     &LAMBD,NQ,QINT)
              USE GLOBAL
              INTEGER(KIND=4) :: NQ
              INTEGER(KIND=4) :: IW
              REAL(KIND=8) :: Z_INI
              REAL(KIND=8) :: Z_L
              REAL(KIND=8) :: T_L
              REAL(KIND=8) :: COSNM(NNM)
              REAL(KIND=8) :: SINNM(NNM)
              REAL(KIND=8) :: LAMBD
              REAL(KIND=8) :: QINT(NQ)
            END SUBROUTINE BOUNCE_INTEGRAND
          END INTERFACE 
        END MODULE BOUNCE_INTEGRAND__genmod
