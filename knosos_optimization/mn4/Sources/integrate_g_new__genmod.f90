        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:28 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INTEGRATE_G_NEW__genmod
          INTERFACE 
            SUBROUTINE INTEGRATE_G_NEW(NALPHA,NALPHAB,NLAMBDA,LAMBDA,I_P&
     &,NPOINT,G,NOTG,THETAP,B_AL,VDS_AL,D11)
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
              REAL(KIND=8) :: G(NPOINT)
              LOGICAL(KIND=4) :: NOTG
              REAL(KIND=8) :: THETAP(NALPHA,NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: VDS_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: D11
            END SUBROUTINE INTEGRATE_G_NEW
          END INTERFACE 
        END MODULE INTEGRATE_G_NEW__genmod
