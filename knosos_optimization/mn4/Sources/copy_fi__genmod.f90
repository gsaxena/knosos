        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:19 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COPY_FI__genmod
          INTERFACE 
            SUBROUTINE COPY_FI(S_ORI,ALPHA_ORI,THETA_ORI,ZETA_ORI,ZL_ORI&
     &,TL_ORI,ZR_ORI,TR_ORI,DB_ORI,J_ORI,DJDS_ORI,DJDA_ORI,DSDT_ORI,    &
     &DADT_ORI,DSDA_ORI,PTRAN_ORI,DBAN_ORI,S_END,ALPHA_END,THETA_END,   &
     &ZETA_END,ZL_END,TL_END,ZR_END,TR_END,DB_END,J_END,DJDS_END,       &
     &DJDA_END,DSDT_END,DADT_END,DSDA_END,PTRAN_END,DBAN_END)
              REAL(KIND=8) :: S_ORI
              REAL(KIND=8) :: ALPHA_ORI
              REAL(KIND=8) :: THETA_ORI
              REAL(KIND=8) :: ZETA_ORI
              REAL(KIND=8) :: ZL_ORI
              REAL(KIND=8) :: TL_ORI
              REAL(KIND=8) :: ZR_ORI
              REAL(KIND=8) :: TR_ORI
              REAL(KIND=8) :: DB_ORI
              REAL(KIND=8) :: J_ORI
              REAL(KIND=8) :: DJDS_ORI
              REAL(KIND=8) :: DJDA_ORI
              REAL(KIND=8) :: DSDT_ORI
              REAL(KIND=8) :: DADT_ORI
              REAL(KIND=8) :: DSDA_ORI
              REAL(KIND=8) :: PTRAN_ORI
              REAL(KIND=8) :: DBAN_ORI
              REAL(KIND=8) :: S_END
              REAL(KIND=8) :: ALPHA_END
              REAL(KIND=8) :: THETA_END
              REAL(KIND=8) :: ZETA_END
              REAL(KIND=8) :: ZL_END
              REAL(KIND=8) :: TL_END
              REAL(KIND=8) :: ZR_END
              REAL(KIND=8) :: TR_END
              REAL(KIND=8) :: DB_END
              REAL(KIND=8) :: J_END
              REAL(KIND=8) :: DJDS_END
              REAL(KIND=8) :: DJDA_END
              REAL(KIND=8) :: DSDT_END
              REAL(KIND=8) :: DADT_END
              REAL(KIND=8) :: DSDA_END
              REAL(KIND=8) :: PTRAN_END
              REAL(KIND=8) :: DBAN_END
            END SUBROUTINE COPY_FI
          END INTERFACE 
        END MODULE COPY_FI__genmod
