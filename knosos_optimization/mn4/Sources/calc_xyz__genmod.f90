        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_XYZ__genmod
          INTERFACE 
            SUBROUTINE CALC_XYZ(S,Z,T,X1,X2,X3,BZT,FLAG_PLOT)
              REAL(KIND=8) :: S
              REAL(KIND=8) :: Z
              REAL(KIND=8) :: T
              REAL(KIND=8) :: X1
              REAL(KIND=8) :: X2
              REAL(KIND=8) :: X3
              REAL(KIND=8) :: BZT
              LOGICAL(KIND=4) :: FLAG_PLOT
            END SUBROUTINE CALC_XYZ
          END INTERFACE 
        END MODULE CALC_XYZ__genmod
