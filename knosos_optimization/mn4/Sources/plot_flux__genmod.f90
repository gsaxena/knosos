        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:50:59 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE PLOT_FLUX__genmod
          INTERFACE 
            SUBROUTINE PLOT_FLUX(JT,JT0,NBB,S,EPSI,GB,QB,L1B,L2B,L3B,ZB,&
     &NB,DNBDPSI,TB,DTBDPSI,EPHI1OTSIZE,GRHS,QRHS,L1RHS,L2RHS,L3RHS,GPHI&
     &,QPHI,L1PHI,L2PHI,L3PHI)
              USE GLOBAL
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: JT0
              REAL(KIND=8) :: S
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: GB(NBB)
              REAL(KIND=8) :: QB(NBB)
              REAL(KIND=8) :: L1B(NBB)
              REAL(KIND=8) :: L2B(NBB)
              REAL(KIND=8) :: L3B(NBB)
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPHI1OTSIZE
              REAL(KIND=8) :: GRHS(NBB)
              REAL(KIND=8) :: QRHS(NBB)
              REAL(KIND=8) :: L1RHS(NBB)
              REAL(KIND=8) :: L2RHS(NBB)
              REAL(KIND=8) :: L3RHS(NBB)
              REAL(KIND=8) :: GPHI(NBB,NNMP)
              REAL(KIND=8) :: QPHI(NBB,NNMP)
              REAL(KIND=8) :: L1PHI(NBB,NNMP)
              REAL(KIND=8) :: L2PHI(NBB,NNMP)
              REAL(KIND=8) :: L3PHI(NBB,NNMP)
            END SUBROUTINE PLOT_FLUX
          END INTERFACE 
        END MODULE PLOT_FLUX__genmod
