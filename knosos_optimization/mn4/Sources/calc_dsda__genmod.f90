        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:19 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_DSDA__genmod
          INTERFACE 
            SUBROUTINE CALC_DSDA(IT,THETA,ZETA,E_O_MU,ZL,TL,ZR,TR,DB,J, &
     &DJDS,DJDA,DJDLA,DSDT,DADT,DSDA,PTRAN,DBAN)
              USE GLOBAL
              INTEGER(KIND=4) :: IT
              REAL(KIND=8) :: THETA
              REAL(KIND=8) :: ZETA
              REAL(KIND=8) :: E_O_MU
              REAL(KIND=8) :: ZL
              REAL(KIND=8) :: TL
              REAL(KIND=8) :: ZR
              REAL(KIND=8) :: TR
              REAL(KIND=8) :: DB
              REAL(KIND=8) :: J
              REAL(KIND=8) :: DJDS
              REAL(KIND=8) :: DJDA
              REAL(KIND=8) :: DJDLA
              REAL(KIND=8) :: DSDT
              REAL(KIND=8) :: DADT
              REAL(KIND=8) :: DSDA
              REAL(KIND=8) :: PTRAN
              REAL(KIND=8) :: DBAN
            END SUBROUTINE CALC_DSDA
          END INTERFACE 
        END MODULE CALC_DSDA__genmod
