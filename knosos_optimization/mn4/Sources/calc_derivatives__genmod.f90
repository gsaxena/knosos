        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:46 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_DERIVATIVES__genmod
          INTERFACE 
            SUBROUTINE CALC_DERIVATIVES(NALPHAB,ZETA,THETA,PHI1,B,      &
     &DPHI1DZ,DPHI1DT,DBDZ,DBDT)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: PHI1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DPHI1DZ(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DPHI1DT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DBDZ(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DBDT(NALPHAB,NALPHAB)
            END SUBROUTINE CALC_DERIVATIVES
          END INTERFACE 
        END MODULE CALC_DERIVATIVES__genmod
