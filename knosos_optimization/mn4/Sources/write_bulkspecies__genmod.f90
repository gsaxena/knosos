        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:50:59 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE WRITE_BULKSPECIES__genmod
          INTERFACE 
            SUBROUTINE WRITE_BULKSPECIES(S,NALPHAB,Q,FILENAME)
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: S
              REAL(KIND=8) :: Q(NALPHAB,NALPHAB)
              CHARACTER(LEN=3) :: FILENAME
            END SUBROUTINE WRITE_BULKSPECIES
          END INTERFACE 
        END MODULE WRITE_BULKSPECIES__genmod
