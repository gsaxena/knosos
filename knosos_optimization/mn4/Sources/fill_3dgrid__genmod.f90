        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_3DGRID__genmod
          INTERFACE 
            SUBROUTINE FILL_3DGRID(NZ,NT,S,X1,X2,X3,BZT,FLAG)
              INTEGER(KIND=4) :: NT
              INTEGER(KIND=4) :: NZ
              REAL(KIND=8) :: S
              REAL(KIND=8) :: X1(NZ,NT)
              REAL(KIND=8) :: X2(NZ,NT)
              REAL(KIND=8) :: X3(NZ,NT)
              REAL(KIND=8) :: BZT(NZ,NT)
              LOGICAL(KIND=4) :: FLAG
            END SUBROUTINE FILL_3DGRID
          END INTERFACE 
        END MODULE FILL_3DGRID__genmod
