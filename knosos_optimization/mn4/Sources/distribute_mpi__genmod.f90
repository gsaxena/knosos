        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:43 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE DISTRIBUTE_MPI__genmod
          INTERFACE 
            SUBROUTINE DISTRIBUTE_MPI(NS,RANK)
              USE GLOBAL
              INTEGER(KIND=4) :: NS
              INTEGER(KIND=4) :: RANK(NS,NERR)
            END SUBROUTINE DISTRIBUTE_MPI
          END INTERFACE 
        END MODULE DISTRIBUTE_MPI__genmod
