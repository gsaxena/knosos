        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:28 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_MATRIX_PETSC__genmod
          INTERFACE 
            SUBROUTINE FILL_MATRIX_PETSC(MATCOL,JV,EPSI,MATVEAF,MATVEAB,&
     &MATVMAF,MATVMAB,KSP)
              USE PETSCKSP
              TYPE (TMAT) :: MATCOL
              INTEGER(KIND=4) :: JV
              REAL(KIND=8) :: EPSI
              TYPE (TMAT) :: MATVEAF
              TYPE (TMAT) :: MATVEAB
              TYPE (TMAT) :: MATVMAF
              TYPE (TMAT) :: MATVMAB
              TYPE (TKSP) :: KSP
            END SUBROUTINE FILL_MATRIX_PETSC
          END INTERFACE 
        END MODULE FILL_MATRIX_PETSC__genmod
