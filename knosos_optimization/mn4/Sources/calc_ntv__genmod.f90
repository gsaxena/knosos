        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:06 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_NTV__genmod
          INTERFACE 
            SUBROUTINE CALC_NTV(JT,S,IB,REGB,ZB,AB,NB,DNBDPSI,TB,DTBDPSI&
     &,EPSI,L1B,GB)
              INTEGER(KIND=4) :: JT
              REAL(KIND=8) :: S
              INTEGER(KIND=4) :: IB
              INTEGER(KIND=4) :: REGB
              REAL(KIND=8) :: ZB
              REAL(KIND=8) :: AB
              REAL(KIND=8) :: NB
              REAL(KIND=8) :: DNBDPSI
              REAL(KIND=8) :: TB
              REAL(KIND=8) :: DTBDPSI
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: L1B
              REAL(KIND=8) :: GB
            END SUBROUTINE CALC_NTV
          END INTERFACE 
        END MODULE CALC_NTV__genmod
