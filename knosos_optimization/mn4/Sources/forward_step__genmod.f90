        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:19 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FORWARD_STEP__genmod
          INTERFACE 
            SUBROUTINE FORWARD_STEP(IT,S,ALPHA,THETA,ZETA,E_O_MU,ZL,TL, &
     &ZR,TR,DB,J,DJDS,DJDA,DSDT,DADT,DSDA,DS,DA,DST,DAT,STEP_A,SMIN,SMAX&
     &,FD,S_NEW,ALPHA_NEW,THETA_NEW,ZETA_NEW,ZL_NEW,TL_NEW,ZR_NEW,TR_NEW&
     &,DB_NEW,J_NEW,TRANSITION,J0,DJDS_NEW,DJDA_NEW,DSDT_NEW,DADT_NEW,  &
     &DSDA_NEW,PTRAN_NEW,DBAN_NEW)
              INTEGER(KIND=4) :: IT
              REAL(KIND=8) :: S
              REAL(KIND=8) :: ALPHA
              REAL(KIND=8) :: THETA
              REAL(KIND=8) :: ZETA
              REAL(KIND=8) :: E_O_MU
              REAL(KIND=8) :: ZL
              REAL(KIND=8) :: TL
              REAL(KIND=8) :: ZR
              REAL(KIND=8) :: TR
              REAL(KIND=8) :: DB
              REAL(KIND=8) :: J
              REAL(KIND=8) :: DJDS
              REAL(KIND=8) :: DJDA
              REAL(KIND=8) :: DSDT
              REAL(KIND=8) :: DADT
              REAL(KIND=8) :: DSDA
              REAL(KIND=8) :: DS
              REAL(KIND=8) :: DA
              REAL(KIND=8) :: DST
              REAL(KIND=8) :: DAT
              LOGICAL(KIND=4) :: STEP_A
              REAL(KIND=8) :: SMIN
              REAL(KIND=8) :: SMAX
              REAL(KIND=8) :: FD
              REAL(KIND=8) :: S_NEW
              REAL(KIND=8) :: ALPHA_NEW
              REAL(KIND=8) :: THETA_NEW
              REAL(KIND=8) :: ZETA_NEW
              REAL(KIND=8) :: ZL_NEW
              REAL(KIND=8) :: TL_NEW
              REAL(KIND=8) :: ZR_NEW
              REAL(KIND=8) :: TR_NEW
              REAL(KIND=8) :: DB_NEW
              REAL(KIND=8) :: J_NEW
              LOGICAL(KIND=4) :: TRANSITION
              REAL(KIND=8) :: J0
              REAL(KIND=8) :: DJDS_NEW
              REAL(KIND=8) :: DJDA_NEW
              REAL(KIND=8) :: DSDT_NEW
              REAL(KIND=8) :: DADT_NEW
              REAL(KIND=8) :: DSDA_NEW
              REAL(KIND=8) :: PTRAN_NEW
              REAL(KIND=8) :: DBAN_NEW
            END SUBROUTINE FORWARD_STEP
          END INTERFACE 
        END MODULE FORWARD_STEP__genmod
