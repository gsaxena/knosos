        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FFTF_KN__genmod
          INTERFACE 
            SUBROUTINE FFTF_KN(NALPHAB,Q,QNM)
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: Q(NALPHAB,NALPHAB)
              COMPLEX(KIND=8) :: QNM(NALPHAB,NALPHAB)
            END SUBROUTINE FFTF_KN
          END INTERFACE 
        END MODULE FFTF_KN__genmod
