        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:45 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_PLATEAU__genmod
          INTERFACE 
            SUBROUTINE CALC_PLATEAU(JV,EPSI,D11,DN1NM)
              USE GLOBAL
              INTEGER(KIND=4) :: JV
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: D11(NNMP,NNMP)
              REAL(KIND=8) :: DN1NM(NNMP,NNMP)
            END SUBROUTINE CALC_PLATEAU
          END INTERFACE 
        END MODULE CALC_PLATEAU__genmod
