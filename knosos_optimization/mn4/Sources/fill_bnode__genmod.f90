        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_BNODE__genmod
          INTERFACE 
            SUBROUTINE FILL_BNODE(ZETA,THETA,JAC,BZT,VDS_BZT,FLAGB1)
              USE GLOBAL
              REAL(KIND=8) :: ZETA
              REAL(KIND=8) :: THETA
              REAL(KIND=8) :: JAC
              REAL(KIND=8) :: BZT
              REAL(KIND=8) :: VDS_BZT(NNMP)
              LOGICAL(KIND=4) :: FLAGB1
            END SUBROUTINE FILL_BNODE
          END INTERFACE 
        END MODULE FILL_BNODE__genmod
