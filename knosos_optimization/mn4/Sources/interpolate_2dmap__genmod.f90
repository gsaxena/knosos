        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INTERPOLATE_2DMAP__genmod
          INTERFACE 
            SUBROUTINE INTERPOLATE_2DMAP(NPT,ZETA_I,THETA_I,FUNC_I,     &
     &FUNC_O)
              INTEGER(KIND=4) :: NPT
              REAL(KIND=8) :: ZETA_I(NPT,NPT)
              REAL(KIND=8) :: THETA_I(NPT,NPT)
              REAL(KIND=8) :: FUNC_I(NPT,NPT)
              REAL(KIND=8) :: FUNC_O(NPT,NPT)
            END SUBROUTINE INTERPOLATE_2DMAP
          END INTERFACE 
        END MODULE INTERPOLATE_2DMAP__genmod
