        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:08 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE EXTREME_POINT__genmod
          INTERFACE 
            SUBROUTINE EXTREME_POINT(Z_IN,T_IN,FLAG_IN,Z_OUT,T_OUT,B_OUT&
     &,HBPP_OUT,VD,FLAG_OUT)
              USE GLOBAL
              REAL(KIND=8) :: Z_IN
              REAL(KIND=8) :: T_IN
              INTEGER(KIND=4) :: FLAG_IN
              REAL(KIND=8) :: Z_OUT
              REAL(KIND=8) :: T_OUT
              REAL(KIND=8) :: B_OUT
              REAL(KIND=8) :: HBPP_OUT
              REAL(KIND=8) :: VD(3)
              INTEGER(KIND=4) :: FLAG_OUT
            END SUBROUTINE EXTREME_POINT
          END INTERFACE 
        END MODULE EXTREME_POINT__genmod
