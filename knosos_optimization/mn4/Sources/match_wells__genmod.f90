        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:08 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE MATCH_WELLS__genmod
          INTERFACE 
            SUBROUTINE MATCH_WELLS(KW,Z1_I,T1_I,B1_I,HBPP1_I,VD1_I,ZB_I,&
     &TB_I,BB_I,HBPPB_I,VDB_I,Z2_I,T2_I,B2_I,HBPP2_I,VD2_I,Z1_II,T1_II, &
     &B1_II,HBPP1_II,VD1_II,ZB_II,TB_II,BB_II,HBPPB_II,VDB_II,Z2_II,    &
     &T2_II,B2_II,HBPP2_II,VD2_II,Z1_III,T1_III,B1_III,HBPP1_III,VD1_III&
     &,ZB_III,TB_III,BB_III,HBPPB_III,VDB_III,Z2_III,T2_III,B2_III,     &
     &HBPP2_III,VD2_III,FLAG_IN,FLAG_OUT)
              INTEGER(KIND=4) :: KW
              REAL(KIND=8) :: Z1_I
              REAL(KIND=8) :: T1_I
              REAL(KIND=8) :: B1_I
              REAL(KIND=8) :: HBPP1_I
              REAL(KIND=8) :: VD1_I(3)
              REAL(KIND=8) :: ZB_I
              REAL(KIND=8) :: TB_I
              REAL(KIND=8) :: BB_I
              REAL(KIND=8) :: HBPPB_I
              REAL(KIND=8) :: VDB_I(3)
              REAL(KIND=8) :: Z2_I
              REAL(KIND=8) :: T2_I
              REAL(KIND=8) :: B2_I
              REAL(KIND=8) :: HBPP2_I
              REAL(KIND=8) :: VD2_I(3)
              REAL(KIND=8) :: Z1_II
              REAL(KIND=8) :: T1_II
              REAL(KIND=8) :: B1_II
              REAL(KIND=8) :: HBPP1_II
              REAL(KIND=8) :: VD1_II(3)
              REAL(KIND=8) :: ZB_II
              REAL(KIND=8) :: TB_II
              REAL(KIND=8) :: BB_II
              REAL(KIND=8) :: HBPPB_II
              REAL(KIND=8) :: VDB_II(3)
              REAL(KIND=8) :: Z2_II
              REAL(KIND=8) :: T2_II
              REAL(KIND=8) :: B2_II
              REAL(KIND=8) :: HBPP2_II
              REAL(KIND=8) :: VD2_II(3)
              REAL(KIND=8) :: Z1_III
              REAL(KIND=8) :: T1_III
              REAL(KIND=8) :: B1_III
              REAL(KIND=8) :: HBPP1_III
              REAL(KIND=8) :: VD1_III(3)
              REAL(KIND=8) :: ZB_III
              REAL(KIND=8) :: TB_III
              REAL(KIND=8) :: BB_III
              REAL(KIND=8) :: HBPPB_III
              REAL(KIND=8) :: VDB_III(3)
              REAL(KIND=8) :: Z2_III
              REAL(KIND=8) :: T2_III
              REAL(KIND=8) :: B2_III
              REAL(KIND=8) :: HBPP2_III
              REAL(KIND=8) :: VD2_III(3)
              INTEGER(KIND=4) :: FLAG_IN
              INTEGER(KIND=4) :: FLAG_OUT
            END SUBROUTINE MATCH_WELLS
          END INTERFACE 
        END MODULE MATCH_WELLS__genmod
