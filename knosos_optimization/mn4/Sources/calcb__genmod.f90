        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALCB__genmod
          INTERFACE 
            SUBROUTINE CALCB(Z,T,FLAG,FLAGB1,B_0,DBDZ_0,DBDT_0,DBDPSI,  &
     &HBPP,B_1,DBDZ_1,DBDT_1,PHI_1,DPHDZ,DPHDT,VDE)
              USE GLOBAL
              REAL(KIND=8) :: Z
              REAL(KIND=8) :: T
              INTEGER(KIND=4) :: FLAG
              LOGICAL(KIND=4) :: FLAGB1
              REAL(KIND=8) :: B_0
              REAL(KIND=8) :: DBDZ_0
              REAL(KIND=8) :: DBDT_0
              REAL(KIND=8) :: DBDPSI
              REAL(KIND=8) :: HBPP
              REAL(KIND=8) :: B_1
              REAL(KIND=8) :: DBDZ_1
              REAL(KIND=8) :: DBDT_1
              REAL(KIND=8) :: PHI_1
              REAL(KIND=8) :: DPHDZ
              REAL(KIND=8) :: DPHDT
              REAL(KIND=8) :: VDE(NNMP)
            END SUBROUTINE CALCB
          END INTERFACE 
        END MODULE CALCB__genmod
