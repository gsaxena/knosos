        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE SORT_ALPHA__genmod
          INTERFACE 
            SUBROUTINE SORT_ALPHA(NALPHA,NALPHAB,ALPHAP,ZETAX,THETAX,   &
     &THETAP,B_AL,VDS_AL,J_AL,NLAMBDA,I_P)
              USE GLOBAL
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              REAL(KIND=8) :: ALPHAP(NALPHA)
              REAL(KIND=8) :: ZETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETAP(NALPHA,NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: VDS_AL(NNMP,NALPHA,NALPHAB)
              INTEGER(KIND=4) :: J_AL(NALPHA,NALPHAB)
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
            END SUBROUTINE SORT_ALPHA
          END INTERFACE 
        END MODULE SORT_ALPHA__genmod
