        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:17 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE AVERAGE_SAMPLES__genmod
          INTERFACE 
            SUBROUTINE AVERAGE_SAMPLES(NBB,NS,S,EPSI,GB,QB)
              USE GLOBAL
              INTEGER(KIND=4) :: NS
              INTEGER(KIND=4) :: NBB
              REAL(KIND=8) :: S(NS)
              REAL(KIND=8) :: EPSI(NS,NERR)
              REAL(KIND=8) :: GB(NBB,NS,NERR)
              REAL(KIND=8) :: QB(NBB,NS,NERR)
            END SUBROUTINE AVERAGE_SAMPLES
          END INTERFACE 
        END MODULE AVERAGE_SAMPLES__genmod
