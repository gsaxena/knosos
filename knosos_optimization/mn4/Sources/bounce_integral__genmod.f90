        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:08 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE BOUNCE_INTEGRAL__genmod
          INTERFACE 
            SUBROUTINE BOUNCE_INTEGRAL(IW,Z_INI,T_INI,Z_FIN,T_FIN,LAMBD,&
     &BP_INI,HBPP_INI,VD_INI,BP_FIN,HBPP_FIN,VD_FIN,Z_BOT,B_BOT,HBPP_BOT&
     &,VD_BOT,NQ,Q)
              USE GLOBAL
              INTEGER(KIND=4) :: NQ
              INTEGER(KIND=4) :: IW
              REAL(KIND=8) :: Z_INI
              REAL(KIND=8) :: T_INI
              REAL(KIND=8) :: Z_FIN
              REAL(KIND=8) :: T_FIN
              REAL(KIND=8) :: LAMBD
              REAL(KIND=8) :: BP_INI
              REAL(KIND=8) :: HBPP_INI
              REAL(KIND=8) :: VD_INI(3)
              REAL(KIND=8) :: BP_FIN
              REAL(KIND=8) :: HBPP_FIN
              REAL(KIND=8) :: VD_FIN(3)
              REAL(KIND=8) :: Z_BOT
              REAL(KIND=8) :: B_BOT
              REAL(KIND=8) :: HBPP_BOT
              REAL(KIND=8) :: VD_BOT(3)
              REAL(KIND=8) :: Q(NQ)
            END SUBROUTINE BOUNCE_INTEGRAL
          END INTERFACE 
        END MODULE BOUNCE_INTEGRAL__genmod
