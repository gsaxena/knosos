        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:28 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_DKE_ROW__genmod
          INTERFACE 
            SUBROUTINE FILL_DKE_ROW(IPOINT,NPOINT,DALPHA_AM1,DALPHA_AP1,&
     &DALPHA_AM2,DALPHA_AP2,I_P_AM1,I_P_AP1,I_P_AM2,I_P_AP2,I_P_AM1I,   &
     &I_P_AM1II,I_P_AM2I,I_P_AM2II,I_P_AM2III,I_P_AM2IV,I_P_AP1I,       &
     &I_P_AP1II,I_P_AP2I,I_P_AP2II,I_P_AP2III,I_P_AP2IV,WM1I,WM1II,WM2I,&
     &WM2II,WM2III,WM2IV,WP1I,WP1II,WP2I,WP2II,WP2III,WP2IV,LAMBDA,     &
     &DLAMBDA_LM1,DLAMBDA_LP1,I_P_LM1,NBIF,NBIFX,I_P_LP1,BI1,BI2,BI4,BI5&
     &,BI3OBI7,MATCOL,MATVEAF,MATVEAB,MATVMAF,MATVMAB,NNZ,FLAG_NNZ,     &
     &MATDIFF,MATDIFB)
              INTEGER(KIND=4) :: NBIFX
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: IPOINT
              REAL(KIND=8) :: DALPHA_AM1
              REAL(KIND=8) :: DALPHA_AP1
              REAL(KIND=8) :: DALPHA_AM2
              REAL(KIND=8) :: DALPHA_AP2
              INTEGER(KIND=4) :: I_P_AM1
              INTEGER(KIND=4) :: I_P_AP1
              INTEGER(KIND=4) :: I_P_AM2
              INTEGER(KIND=4) :: I_P_AP2
              INTEGER(KIND=4) :: I_P_AM1I
              INTEGER(KIND=4) :: I_P_AM1II
              INTEGER(KIND=4) :: I_P_AM2I
              INTEGER(KIND=4) :: I_P_AM2II
              INTEGER(KIND=4) :: I_P_AM2III
              INTEGER(KIND=4) :: I_P_AM2IV
              INTEGER(KIND=4) :: I_P_AP1I
              INTEGER(KIND=4) :: I_P_AP1II
              INTEGER(KIND=4) :: I_P_AP2I
              INTEGER(KIND=4) :: I_P_AP2II
              INTEGER(KIND=4) :: I_P_AP2III
              INTEGER(KIND=4) :: I_P_AP2IV
              REAL(KIND=8) :: WM1I
              REAL(KIND=8) :: WM1II
              REAL(KIND=8) :: WM2I
              REAL(KIND=8) :: WM2II
              REAL(KIND=8) :: WM2III
              REAL(KIND=8) :: WM2IV
              REAL(KIND=8) :: WP1I
              REAL(KIND=8) :: WP1II
              REAL(KIND=8) :: WP2I
              REAL(KIND=8) :: WP2II
              REAL(KIND=8) :: WP2III
              REAL(KIND=8) :: WP2IV
              REAL(KIND=8) :: LAMBDA
              REAL(KIND=8) :: DLAMBDA_LM1(NPOINT)
              REAL(KIND=8) :: DLAMBDA_LP1(NPOINT)
              INTEGER(KIND=4) :: I_P_LM1(NPOINT)
              INTEGER(KIND=4) :: NBIF(NPOINT)
              INTEGER(KIND=4) :: I_P_LP1(NBIFX,NPOINT)
              REAL(KIND=8) :: BI1
              REAL(KIND=8) :: BI2(NPOINT)
              REAL(KIND=8) :: BI4
              REAL(KIND=8) :: BI5
              REAL(KIND=8) :: BI3OBI7
              REAL(KIND=8) :: MATCOL(NPOINT)
              REAL(KIND=8) :: MATVEAF(NPOINT)
              REAL(KIND=8) :: MATVEAB(NPOINT)
              REAL(KIND=8) :: MATVMAF(NPOINT)
              REAL(KIND=8) :: MATVMAB(NPOINT)
              INTEGER(KIND=4) :: NNZ
              LOGICAL(KIND=4) :: FLAG_NNZ
              REAL(KIND=8) :: MATDIFF(NPOINT)
              REAL(KIND=8) :: MATDIFB(NPOINT)
            END SUBROUTINE FILL_DKE_ROW
          END INTERFACE 
        END MODULE FILL_DKE_ROW__genmod
