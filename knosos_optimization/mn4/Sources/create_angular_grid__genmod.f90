        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CREATE_ANGULAR_GRID__genmod
          INTERFACE 
            SUBROUTINE CREATE_ANGULAR_GRID(NA,NALPHA,NALPHAB,ALPHAP,    &
     &DALPHAP,OFFSET,ZETAP,THETAP,ZETAX,THETAX,B_AL,VDS_AL,J_AL)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              INTEGER(KIND=4) :: NA
              REAL(KIND=8) :: ALPHAP(NALPHA)
              REAL(KIND=8) :: DALPHAP(NTURN+1)
              REAL(KIND=8) :: OFFSET
              REAL(KIND=8) :: ZETAP(NALPHAB)
              REAL(KIND=8) :: THETAP(NALPHA,NALPHAB)
              REAL(KIND=8) :: ZETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: VDS_AL(NNMP,NALPHA,NALPHAB)
              INTEGER(KIND=4) :: J_AL(NALPHA,NALPHAB)
            END SUBROUTINE CREATE_ANGULAR_GRID
          END INTERFACE 
        END MODULE CREATE_ANGULAR_GRID__genmod
