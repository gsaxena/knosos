        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:46 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE ANISOTROPY_TRACE_PS__genmod
          INTERFACE 
            SUBROUTINE ANISOTROPY_TRACE_PS(JT,ZZ,AI,TZ,NALPHAB,B,DBDZ,  &
     &DBDT,DPHI1DZ,DPHI1DT,MBB,TRM,GB)
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: JT
              REAL(KIND=8) :: ZZ
              REAL(KIND=8) :: AI
              REAL(KIND=8) :: TZ
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DBDZ(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DBDT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DPHI1DZ(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DPHI1DT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: MBB(NALPHAB,NALPHAB)
              REAL(KIND=8) :: TRM(NALPHAB,NALPHAB)
              REAL(KIND=8) :: GB
            END SUBROUTINE ANISOTROPY_TRACE_PS
          END INTERFACE 
        END MODULE ANISOTROPY_TRACE_PS__genmod
