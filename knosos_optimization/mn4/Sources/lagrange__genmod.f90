        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:51:27 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE LAGRANGE__genmod
          INTERFACE 
            SUBROUTINE LAGRANGE(XA,YA,N,X,Y,ORDER)
              INTEGER(KIND=4) :: N
              REAL(KIND=8) :: XA(N)
              REAL(KIND=8) :: YA(N)
              REAL(KIND=8) :: X
              REAL(KIND=8) :: Y
              INTEGER(KIND=4) :: ORDER
            END SUBROUTINE LAGRANGE
          END INTERFACE 
        END MODULE LAGRANGE__genmod
