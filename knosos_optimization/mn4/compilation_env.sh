export LD_LIBRARY_PATH=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib:$LIBRARY_PATH
export C_INCLUDE_PATH=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/include:$CPLUS_INCLUDE_PATH
export PKG_CONFIG_PATH=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib/pkgconfig:$PKG_CONFIG_PATH
export PETSC_DIR=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build
export PETSC_ARCH=linux-x86_64-opt

module purge
module load intel/2018.4 impi/2018.4 mkl/2018.4 netcdf fftw
