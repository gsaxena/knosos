export LD_LIBRARY_PATH=/gpfs/projects/bsc99/bsc99204/KNOSOS/petsc-3.16.1_tuned_nord3/build/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=/gpfs/projects/bsc99/bsc99204/KNOSOS/petsc-3.16.1_tuned_nord3/build/lib:$LIBRARY_PATH
export C_INCLUDE_PATH=/gpfs/projects/bsc99/bsc99204/KNOSOS/petsc-3.16.1_tuned_nord3/build/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=/gpfs/projects/bsc99/bsc99204/KNOSOS/petsc-3.16.1_tuned_nord3/build/include:$CPLUS_INCLUDE_PATH
export PKG_CONFIG_PATH=/gpfs/projects/bsc99/bsc99204/KNOSOS/petsc-3.16.1_tuned_nord3/build/lib/pkgconfig:$PKG_CONFIG_PATH
export PETSC_DIR=/gpfs/projects/bsc99/bsc99204/KNOSOS/petsc-3.16.1_tuned_nord3/build
export PETSC_ARCH=linux-x86_64-opt

module purge
module load intel impi mkl netcdf-c/4.4.1.1_hdf5-1.10.8 netcdf-fortran/4.4.4 fftw
