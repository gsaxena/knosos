#!/usr/bin/python3
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--PETSC_DIR=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned',
    '--prefix=/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build',
    '--with-64-bit-indices=1',
    '--with-avx512-kernels=1',
    '--with-blaslapack-lib=/apps/INTEL/2018.4.057/mkl/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group /apps/INTEL/2018.4.057/mkl/lib/intel64/libmkl_intel_lp64.a /apps/INTEL/2018.4.057/mkl/lib/intel64/libmkl_intel_thread.a /apps/INTEL/2018.4.057/mkl/lib/intel64/libmkl_core.a /apps/INTEL/2018.4.057/mkl/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group -liomp5 -lpthread -ldl',
    '--with-cc=mpiicc',
    '--with-cxx=mpiicpc',
    '--with-debugging=0',
    '--with-fc=mpiifort',
    '--with-petsc-arch=linux-x86_64-opt',
    '--with-scalar-type=real',
    'CFLAGS=-I/apps/NETCDF/4.4.1.1/INTEL/IMPI/include',
    'COPTFLAGS=-g -O',
    'CXXFLAGS=-I/apps/NETCDF/4.4.1.1/INTEL/IMPI/include',
    'CXXOPTFLAGS=-g -O',
    'FCFLAGS=-I/apps/NETCDF/4.4.1.1/INTEL/IMPI/include',
    'FOPTFLAGS=-g -O',
    'PETSC_ARCH=linux-x86_64-opt',
  ]
  configure.petsc_configure(configure_options)
