static const char *petscmachineinfo = "\n"
"-----------------------------------------\n"
"Libraries compiled on 2022-09-12 06:54:47 on login3 \n"
"Machine characteristics: Linux-4.4.120-92.70-default-x86_64-with-SuSE-12-x86_64\n"
"Using PETSc directory: /gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build\n"
"Using PETSc arch: \n"
"-----------------------------------------\n";
static const char *petsccompilerinfo = "\n"
"Using C compiler: mpiicc -I/apps/NETCDF/4.4.1.1/INTEL/IMPI/include -fPIC -g -O  -std=c99 \n"
"Using Fortran compiler: mpiifort  -fPIC -g -O     -std=c99\n"
"-----------------------------------------\n";
static const char *petsccompilerflagsinfo = "\n"
"Using include paths: -I/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/include\n"
"-----------------------------------------\n";
static const char *petsclinkerinfo = "\n"
"Using C linker: mpiicc\n"
"Using Fortran linker: mpiifort\n"
"Using libraries: -Wl,-rpath,/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib -L/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib -lpetsc -Wl,-rpath,/apps/INTEL/2018.4.057/mkl/lib/intel64 -L/apps/INTEL/2018.4.057/mkl/lib/intel64 -Wl,-rpath,/apps/INTEL/2018.4.057/impi/2018.4.274/intel64/lib/debug_mt -L/apps/INTEL/2018.4.057/impi/2018.4.274/intel64/lib/debug_mt -Wl,-rpath,/apps/INTEL/2018.4.057/impi/2018.4.274/intel64/lib -L/apps/INTEL/2018.4.057/impi/2018.4.274/intel64/lib -Wl,-rpath,/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib -L/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned/build/lib -Wl,-rpath,/gpfs/apps/MN4/INTEL/2018.4.057/compilers_and_libraries_2018.5.274/linux/compiler/lib/intel64_lin -L/gpfs/apps/MN4/INTEL/2018.4.057/compilers_and_libraries_2018.5.274/linux/compiler/lib/intel64_lin -Wl,-rpath,/usr/lib64/gcc/x86_64-suse-linux/4.8 -L/usr/lib64/gcc/x86_64-suse-linux/4.8 -Wl,-rpath,/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned -L/gpfs/projects/bsc99/bsc99206/KNOSOS/petsc-3.16.1_tuned -Wl,-rpath,/usr/x86_64-suse-linux/lib -L/usr/x86_64-suse-linux/lib -Wl,-rpath,/opt/intel/mpi-rt/2017.0.0/intel64/lib/debug_mt -Wl,-rpath,/opt/intel/mpi-rt/2017.0.0/intel64/lib -lmkl_scalapack_lp64 -Wl,--start-group -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -Wl,--end-group -liomp5 -lpthread -ldl -lX11 -lstdc++ -ldl -lmpifort -lmpi -lmpigi -lrt -lpthread -lifport -lifcoremt_pic -limf -lsvml -lm -lipgo -lirc -lgcc_s -lirc_s -lquadmath -lstdc++ -ldl\n"
"-----------------------------------------\n";
