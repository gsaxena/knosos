#!/bin/bash
#SBATCH --job-name=knosos
#SBATCH --output=%j.out
#SBATCH --error=%j.err 
#SBATCH --ntasks=10 
##SBATCH --x11=batch

#source /gpfs/projects/bsc99/bsc99204/KNOSOS/compilation_env.sh

module load scorep/5.0
module load scalasca/2.5

#time scalasca -analyze srun knosos.x 
#time srun knosos.x
#ddt --connect srun knosos.x
ddt srun --x11 knosos.x 
