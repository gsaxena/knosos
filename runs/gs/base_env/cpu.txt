Intel(R) processor family information utility, Version 2018 Update 4 Build 20180823 (id: 18555)
Copyright (C) 2005-2018 Intel Corporation.  All rights reserved.

=====  Processor composition  =====
Processor name    : Intel(R) Xeon(R) Platinum 8160  
Packages(sockets) : 2
Cores             : 48
Processors(CPUs)  : 48
Cores per package : 24
Threads per core  : 1

=====  Processor identification  =====
Processor	Thread Id.	Core Id.	Package Id.
0       	0   		0   		0   
1       	0   		1   		0   
2       	0   		2   		0   
3       	0   		3   		0   
4       	0   		4   		0   
5       	0   		5   		0   
6       	0   		8   		0   
7       	0   		9   		0   
8       	0   		10  		0   
9       	0   		11  		0   
10      	0   		12  		0   
11      	0   		13  		0   
12      	0   		16  		0   
13      	0   		17  		0   
14      	0   		18  		0   
15      	0   		19  		0   
16      	0   		20  		0   
17      	0   		21  		0   
18      	0   		24  		0   
19      	0   		25  		0   
20      	0   		26  		0   
21      	0   		27  		0   
22      	0   		28  		0   
23      	0   		29  		0   
24      	0   		0   		1   
25      	0   		1   		1   
26      	0   		2   		1   
27      	0   		3   		1   
28      	0   		4   		1   
29      	0   		5   		1   
30      	0   		8   		1   
31      	0   		9   		1   
32      	0   		10  		1   
33      	0   		11  		1   
34      	0   		12  		1   
35      	0   		13  		1   
36      	0   		16  		1   
37      	0   		17  		1   
38      	0   		18  		1   
39      	0   		19  		1   
40      	0   		20  		1   
41      	0   		21  		1   
42      	0   		24  		1   
43      	0   		25  		1   
44      	0   		26  		1   
45      	0   		27  		1   
46      	0   		28  		1   
47      	0   		29  		1   
=====  Placement on packages  =====
Package Id.	Core Id.	Processors
0   		0,1,2,3,4,5,8,9,10,11,12,13,16,17,18,19,20,21,24,25,26,27,28,29		0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
1   		0,1,2,3,4,5,8,9,10,11,12,13,16,17,18,19,20,21,24,25,26,27,28,29		24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47

=====  Cache sharing  =====
Cache	Size		Processors
L1	32  KB		no sharing
L2	1   MB		no sharing
L3	33  MB		(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)(24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47)
