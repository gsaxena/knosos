#!/bin/bash
#SBATCH --job-name=knosos
#SBATCH --output=%j.out
#SBATCH --error=%j.err 
#SBATCH --ntasks=22 

#source /gpfs/projects/bsc99/bsc99204/KNOSOS/compilation_env.sh

#time scalasca -analyze "mpiexec -env I_MPI_PIN=1 -env I_MPI_PIN_PROCESSOR_LIST=0,2,4,6,8,10,12,14,16,18,20,24,26,28,30,32,34,36,38,40,42,44 -env I_MPI_DEBUG=4" ./knosos.x 

#time scalasca -analyze srun ./knosos.x

ddt --connect srun ./knosos.x
#srun ./knosos.x -fp_trap
#time srun ./knosos.x 
#srun amplxe-cl -r vtune-knosos-22 -collect hotspots ./knosos.x

