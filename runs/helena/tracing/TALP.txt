######### Monitoring Region POP Metrics #########
### Name:                       MPI Execution
### Elapsed Time :              626.92 s
### Parallel efficiency :       0.13
###   - Communication eff. :    1.00
###   - Load Balance :          0.13
###       - LB_in :             0.13
###       - LB_out:             1.00
######### Monitoring Region POP Raw Data #########
### Name:                       MPI Execution
### Number of CPUs:             8
### Number of nodes:            1
### Elapsed Time:               626924496152 ns
### Elapsed Useful:             626429103864 ns
### Useful CPU Time (Total):    627265683182 ns
### Useful CPU Time (Node):     627265683182 ns
 |----------------------------------------------------------|
 |                  Extended Report Node    0               |
 |----------------------------------------------------------|
 |  Process   |     Useful Time      |       MPI Time       |
 |------------|----------------------|----------------------|
 | 315731     |       6.264291e+02 s |       4.947967e-01 s |
 |------------|----------------------|----------------------|
 | 315732     |       1.441593e-01 s |       6.267789e+02 s |
 |------------|----------------------|----------------------|
 | 315733     |       1.565828e-01 s |       6.267668e+02 s |
 |------------|----------------------|----------------------|
 | 315734     |       9.876518e-02 s |       6.268245e+02 s |
 |------------|----------------------|----------------------|
 | 315735     |       7.794479e-02 s |       6.268466e+02 s |
 |------------|----------------------|----------------------|
 | 315736     |       1.722775e-01 s |       6.267512e+02 s |
 |------------|----------------------|----------------------|
 | 315737     |       9.660387e-02 s |       6.268268e+02 s |
 |------------|----------------------|----------------------|
 | 315738     |       9.024577e-02 s |       6.268330e+02 s |
 |------------|----------------------|----------------------|
 |------------|----------------------|----------------------|
 | Node Avg   |       7.840821e+01 s |       5.485153e+02 s |
 |------------|----------------------|----------------------|
 | Node Max   |       6.264291e+02 s |       6.268466e+02 s |
 |------------|----------------------|----------------------|
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315731 (s20r2b35)
### Rank:                      0
### CpuSet:                    [32]
### Elapsed time :             626.9239 seconds
### Elapsed useful time :      626.429104 seconds
### MPI time :                 0.494796688 seconds
### Useful time :              626.429104 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315732 (s20r2b35)
### Rank:                      1
### CpuSet:                    [33]
### Elapsed time :             626.923027 seconds
### Elapsed useful time :      0.144159326 seconds
### MPI time :                 626.778868 seconds
### Useful time :              0.144159326 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315733 (s20r2b35)
### Rank:                      2
### CpuSet:                    [34]
### Elapsed time :             626.923358 seconds
### Elapsed useful time :      0.156582832 seconds
### MPI time :                 626.766775 seconds
### Useful time :              0.156582832 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315734 (s20r2b35)
### Rank:                      3
### CpuSet:                    [35]
### Elapsed time :             626.923246 seconds
### Elapsed useful time :      0.098765182 seconds
### MPI time :                 626.824481 seconds
### Useful time :              0.098765182 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315735 (s20r2b35)
### Rank:                      4
### CpuSet:                    [36]
### Elapsed time :             626.924496 seconds
### Elapsed useful time :      0.077944794 seconds
### MPI time :                 626.846552 seconds
### Useful time :              0.077944794 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315736 (s20r2b35)
### Rank:                      5
### CpuSet:                    [37]
### Elapsed time :             626.923441 seconds
### Elapsed useful time :      0.172277545 seconds
### MPI time :                 626.751164 seconds
### Useful time :              0.172277545 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315737 (s20r2b35)
### Rank:                      6
### CpuSet:                    [38]
### Elapsed time :             626.923362 seconds
### Elapsed useful time :      0.096603866 seconds
### MPI time :                 626.826759 seconds
### Useful time :              0.096603866 seconds
########### Monitoring Region Summary ###########
### Name:                      MPI Execution
### Process:                   315738 (s20r2b35)
### Rank:                      7
### CpuSet:                    [39]
### Elapsed time :             626.923222 seconds
### Elapsed useful time :      0.090245773 seconds
### MPI time :                 626.832976 seconds
### Useful time :              0.090245773 seconds
