#!/bin/bash
#SBATCH --job-name=knosos-srun
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err 

##SBATCH --reservation=vtune
#SBATCH --constraint=perfparanoid

#SBATCH --ntasks=22
#SBATCH --qos=benchmark
module load intel/2020.0 impi/2018.4 mkl/2018.4 netcdf/4.4.1.1

source /gpfs/projects/bsc99/bsc99206/KNOSOS/knosos/petsc_env.sh

#source $INTEL_HOME/vtune_profiler_2020.0.0.605129/env/vars.sh

ulimit -Ss unlimited

mpirun -n 22 /gpfs/projects/bsc99/bsc99206/KNOSOS/knosos/Sources/knosos2.x
#mpirun -n 22 vtune -collect hotspots -k sampling-mode=sw -trace-mpi -result-dir ./vtune-data -- /gpfs/projects/bsc99/bsc99206/KNOSOS/knosos/Sources/knosos.x
