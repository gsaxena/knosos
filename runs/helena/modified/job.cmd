#!/bin/bash
#SBATCH --job-name=knosos-srun
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err 

#SBATCH --ntasks=22
##SBATCH --partition=main
#SBATCH --qos=benchmark

#ulimit -s unlimited
#ulimit -sS 10240

#module load intel/2018.4 impi/2018.4 mkl/2018.4 petsc/3.15.1-complex hdf5/1.10.5 lapack/3.8.0 netcdf/4.4.1.1
#module load intel/2018.4 impi/2018.4 mkl/2018.4 petsc/3.15.1-real hdf5/1.8.19 netcdf/4.4.1.1
#module load intel/2018.4 impi/2018.4 mkl/2018.4 petsc/3.15.1-real netcdf/4.4.1.1
module load intel/2018.4 impi/2018.4 mkl/2018.4 netcdf/4.4.1.1
source /gpfs/projects/bsc99/bsc99520/KNOSOS/knosos/petsc_env.sh
time srun knosos.x 
