#!/bin/bash
#SBATCH --job-name=knosos-srun-8
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err 
#SBATCH --output=/gpfs/projects/bsc99/bsc99206/KNOSOS/results/%x-%j.out
#SBATCH --error=/gpfs/projects/bsc99/bsc99206/KNOSOS/results/%x-%j.err

#SBATCH --ntasks=8
##SBATCH --partition=main
#SBATCH --qos=benchmark

#ulimit -s unlimited
#ulimit -sS 10240

#module load intel/2018.4 impi/2018.4 mkl/2018.4 petsc/3.15.1-complex hdf5/1.10.5 lapack/3.8.0 netcdf/4.4.1.1
#module load intel/2018.4 impi/2018.4 mkl/2018.4 petsc/3.15.1-real hdf5/1.8.19 netcdf/4.4.1.1
#module load intel/2018.4 impi/2018.4 mkl/2018.4 petsc/3.15.1-real netcdf/4.4.1.1
module load intel/2018.4 impi/2017.4 mkl/2017.4 netcdf/4.4.1.1 

#export DLB_ARGS="$DLB_ARGS --talp --talp-summary=all --talp-output-file=TALP.txt" 

#export LD_PRELOAD=$DLB_HOME/lib/libdlb_mpi.so

source /gpfs/projects/bsc99/bsc99520/KNOSOS/knosos/petsc_env.sh
time mpirun -np 8 ./trace.sh ./knosos.x 
