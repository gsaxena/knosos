#!/bin/bash
#SBATCH --job-name=knosos
#SBATCH --output=%j.out
#SBATCH --error=%j.err 
#SBATCH --ntasks=2 
#SBATCH --partition=main

module load intel/2018.4 impi/2018.4 mkl/2018.4 netcdf/4.4.1.1
source /gpfs/projects/bsc99/bsc99520/KNOSOS/knosos/petsc_env.sh
time srun knosos.x 
