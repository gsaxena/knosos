Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 63.98    119.37   119.37 179692920     0.00     0.00  bounce_integrand_
 10.90    139.71    20.34     4997     0.00     0.01  integrate_g_
  7.31    153.36    13.65 179587802     0.00     0.00  delta_phase_
  6.90    166.23    12.87 107640964     0.00     0.00  lagrange_
  1.96    169.89     3.66    22910     0.00     0.00  calc_plateau_
  1.90    173.43     3.54   210206     0.00     0.00  bounce_point_
  1.50    176.23     2.80     4997     0.00     0.04  calc_low_collisionality_nanl_
  1.49    179.01     2.78   105118     0.00     0.00  bounce_integral_
  1.12    181.10     2.09     7494     0.00     0.00  calc_ps_
  0.72    182.44     1.34     9370     0.00     0.00  extreme_point_
  0.61    183.58     1.14       22     0.05     0.05  find_3dpoints_
  0.42    184.35     0.78   346509     0.00     0.00  calcb_
  0.33    184.97     0.62       30     0.02     0.02  find_alpha_neighbours_
  0.23    185.39     0.42       60     0.01     0.01  label_gridpoints_
  0.20    185.77     0.38       44     0.01     0.01  calc_absnablapsi_
  0.07    185.90     0.13    66912     0.00     0.00  fill_bnode_
  0.07    186.03     0.13      201     0.00     0.00  sort_alpha_
  0.04    186.11     0.08       44     0.00     0.01  fill_bgrid_
  0.04    186.18     0.07     1216     0.00     0.15  calc_fluxes_
  0.03    186.25     0.07                             bounce_integrand_minf_
  0.03    186.31     0.06   105118     0.00     0.00  fsa_
  0.02    186.34     0.03     4997     0.00     0.00  invert_matrix_petsc_
  0.02    186.37     0.03       30     0.00     0.06  characterize_wells_
  0.01    186.39     0.02  1655209     0.00     0.00  match_wells_
  0.01    186.41     0.02       66     0.00     0.00  interpolate_field_
  0.01    186.43     0.02       22     0.00     0.05  jacobian_and_3dgrid_
  0.01    186.45     0.02       22     0.00     0.08  read_bfield_
  0.01    186.47     0.02       22     0.00     0.00  read_boozmnnc_
  0.01    186.49     0.02                             trilagrange_
  0.01    186.50     0.01   202488     0.00     0.00  calculate_time_
  0.01    186.51     0.01     4997     0.00     0.00  fill_matrix_petsc_
  0.01    186.52     0.01       88     0.00     0.00  fill_nm_
  0.01    186.53     0.01       66     0.00     0.00  fill_3dgrid_
  0.01    186.54     0.01       30     0.00     4.66  coefficients_dke_
  0.01    186.55     0.01       30     0.00     0.00  create_lambda_grid_
  0.01    186.56     0.01       30     0.00     0.06  find_wells_
  0.01    186.57     0.01        3     0.00     0.00  find_bmin_bmax_
  0.00    186.57     0.01                             ana_integral_
  0.00    186.57     0.00   210176     0.00     0.00  fill_dke_row_
  0.00    186.57     0.00   105118     0.00     0.00  bounces_
  0.00    186.57     0.00     4997     0.00     0.04  calc_low_collisionality_
  0.00    186.57     0.00     2454     0.00     0.00  dke_constants_
  0.00    186.57     0.00     1216     0.00     0.00  plot_flux_
  0.00    186.57     0.00      132     0.00     0.00  rgauss_
  0.00    186.57     0.00       66     0.00     0.00  read_profile_
  0.00    186.57     0.00       30     0.00     0.00  create_angular_grid_
  0.00    186.57     0.00       30     0.00     0.00  exclude_wells_
  0.00    186.57     0.00       30     0.00     0.00  init_linear_problem_
  0.00    186.57     0.00       22     0.00     0.04  calc_database_
  0.00    186.57     0.00       22     0.00     0.00  check_jacsign_
  0.00    186.57     0.00       22     0.00     0.00  read_addddkesdata_
  0.00    186.57     0.00       22     0.00     0.00  read_boozmndata_
  0.00    186.57     0.00       22     0.00     0.00  read_dkes_table_
  0.00    186.57     0.00       22     0.00     0.00  read_plasmas_
  0.00    186.57     0.00       22     0.00     0.00  read_sources_
  0.00    186.57     0.00       22     0.00     0.00  rescale_
  0.00    186.57     0.00       22     0.00     8.36  solve_dke_qn_amb_
  0.00    186.57     0.00        1     0.00   186.48  MAIN__
  0.00    186.57     0.00        1     0.00     0.00  distribute_mpi_
  0.00    186.57     0.00        1     0.00     0.00  end_all_
  0.00    186.57     0.00        1     0.00     0.00  init_files_
  0.00    186.57     0.00        1     0.00     0.00  init_randomseed_
  0.00    186.57     0.00        1     0.00     0.00  initialize_mpi_
  0.00    186.57     0.00        1     0.00     0.00  read_input_
  0.00    186.57     0.00        1     0.00     0.00  transport_

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.

 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2017 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 0.01% of 186.57 seconds

index % time    self  children    called     name
                0.00  186.48       1/1           main [2]
[1]    100.0    0.00  186.48       1         MAIN__ [1]
                0.00  183.97      22/22          solve_dke_qn_amb_ [3]
                0.02    1.69      22/22          read_bfield_ [17]
                0.00    0.80      22/22          calc_database_ [23]
                0.00    0.00      22/22          read_plasmas_ [47]
                0.00    0.00       1/202488      calculate_time_ [42]
                0.00    0.00      22/22          read_sources_ [58]
                0.00    0.00       1/1           initialize_mpi_ [64]
                0.00    0.00       1/1           read_input_ [65]
                0.00    0.00       1/1           distribute_mpi_ [60]
                0.00    0.00       1/1           init_randomseed_ [63]
                0.00    0.00       1/1           init_files_ [62]
                0.00    0.00       1/1           transport_ [66]
                0.00    0.00       1/1           end_all_ [61]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00  186.48                 main [2]
                0.00  186.48       1/1           MAIN__ [1]
-----------------------------------------------
                0.00  183.97      22/22          MAIN__ [1]
[3]     98.6    0.00  183.97      22         solve_dke_qn_amb_ [3]
                0.07  183.90    1216/1216        calc_fluxes_ [4]
                0.00    0.00      22/202488      calculate_time_ [42]
-----------------------------------------------
                0.07  183.90    1216/1216        solve_dke_qn_amb_ [3]
[4]     98.6    0.07  183.90    1216         calc_fluxes_ [4]
                0.00  178.16    4975/4997        calc_low_collisionality_ [5]
                3.66    0.00   22888/22910       calc_plateau_ [15]
                2.08    0.00    7472/7494        calc_ps_ [16]
                0.00    0.00   71886/202488      calculate_time_ [42]
                0.00    0.00    2432/2454        dke_constants_ [51]
                0.00    0.00    1216/1216        plot_flux_ [52]
-----------------------------------------------
                0.00    0.79      22/4997        calc_database_ [23]
                0.00  178.16    4975/4997        calc_fluxes_ [4]
[5]     95.9    0.00  178.95    4997         calc_low_collisionality_ [5]
                2.80  176.15    4997/4997        calc_low_collisionality_nanl_ [6]
-----------------------------------------------
                2.80  176.15    4997/4997        calc_low_collisionality_ [5]
[6]     95.9    2.80  176.15    4997         calc_low_collisionality_nanl_ [6]
                0.01  139.81      30/30          coefficients_dke_ [7]
               20.34   12.99    4997/4997        integrate_g_ [11]
                0.03    1.67      30/30          characterize_wells_ [18]
                0.62    0.00      30/30          find_alpha_neighbours_ [25]
                0.42    0.00      60/60          label_gridpoints_ [27]
                0.13    0.00     201/201         sort_alpha_ [30]
                0.00    0.07      30/30          create_angular_grid_ [31]
                0.03    0.00    4997/4997        invert_matrix_petsc_ [35]
                0.01    0.00    4997/4997        fill_matrix_petsc_ [39]
                0.01    0.00      30/30          create_lambda_grid_ [40]
                0.00    0.00    4997/202488      calculate_time_ [42]
                0.00    0.00      30/30          exclude_wells_ [48]
                0.00    0.00      30/30          init_linear_problem_ [49]
                0.00    0.00  210176/210176      fill_dke_row_ [50]
-----------------------------------------------
                0.01  139.81      30/30          calc_low_collisionality_nanl_ [6]
[7]     74.9    0.01  139.81      30         coefficients_dke_ [7]
                0.00  139.81  105118/105118      bounces_ [8]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                0.00  139.81  105118/105118      coefficients_dke_ [7]
[8]     74.9    0.00  139.81  105118         bounces_ [8]
                2.78  133.02  105118/105118      bounce_integral_ [9]
                3.54    0.47  210206/210206      bounce_point_ [14]
                0.01    0.00  105118/202488      calculate_time_ [42]
-----------------------------------------------
                2.78  133.02  105118/105118      bounces_ [8]
[9]     72.8    2.78  133.02  105118         bounce_integral_ [9]
              119.37    0.00 179692920/179692920     bounce_integrand_ [10]
               13.65    0.00 179587802/179587802     delta_phase_ [12]
-----------------------------------------------
              119.37    0.00 179692920/179692920     bounce_integral_ [9]
[10]    64.0  119.37    0.00 179692920         bounce_integrand_ [10]
-----------------------------------------------
               20.34   12.99    4997/4997        calc_low_collisionality_nanl_ [6]
[11]    17.9   20.34   12.99    4997         integrate_g_ [11]
               12.87    0.00 107640832/107640964     lagrange_ [13]
                0.06    0.00  105118/105118      fsa_ [33]
                0.06    0.00   30720/66912       fill_bnode_ [29]
                0.00    0.00    4997/202488      calculate_time_ [42]
-----------------------------------------------
               13.65    0.00 179587802/179587802     bounce_integral_ [9]
[12]     7.3   13.65    0.00 179587802         delta_phase_ [12]
-----------------------------------------------
                0.00    0.00     132/107640964     read_profile_ [46]
               12.87    0.00 107640832/107640964     integrate_g_ [11]
[13]     6.9   12.87    0.00 107640964         lagrange_ [13]
-----------------------------------------------
                3.54    0.47  210206/210206      bounces_ [8]
[14]     2.1    3.54    0.47  210206         bounce_point_ [14]
                0.47    0.00  210206/346509      calcb_ [24]
-----------------------------------------------
                0.00    0.00      22/22910       calc_database_ [23]
                3.66    0.00   22888/22910       calc_fluxes_ [4]
[15]     2.0    3.66    0.00   22910         calc_plateau_ [15]
-----------------------------------------------
                0.01    0.00      22/7494        calc_database_ [23]
                2.08    0.00    7472/7494        calc_fluxes_ [4]
[16]     1.1    2.09    0.00    7494         calc_ps_ [16]
-----------------------------------------------
                0.02    1.69      22/22          MAIN__ [1]
[17]     0.9    0.02    1.69      22         read_bfield_ [17]
                0.02    1.15      22/22          jacobian_and_3dgrid_ [21]
                0.08    0.38      44/44          fill_bgrid_ [26]
                0.02    0.02      66/66          interpolate_field_ [34]
                0.02    0.00      22/22          read_boozmnnc_ [37]
                0.00    0.00      22/88          fill_nm_ [43]
                0.00    0.00      22/202488      calculate_time_ [42]
                0.00    0.00      22/22          read_boozmndata_ [56]
                0.00    0.00      22/22          read_addddkesdata_ [55]
                0.00    0.00      22/22          rescale_ [59]
-----------------------------------------------
                0.03    1.67      30/30          calc_low_collisionality_nanl_ [6]
[18]     0.9    0.03    1.67      30         characterize_wells_ [18]
                0.01    1.64      30/30          find_wells_ [19]
                0.02    0.00 1655209/1655209     match_wells_ [36]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                0.01    1.64      30/30          characterize_wells_ [18]
[19]     0.9    0.01    1.64      30         find_wells_ [19]
                1.34    0.30    9370/9370        extreme_point_ [20]
                0.00    0.00      60/346509      calcb_ [24]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                1.34    0.30    9370/9370        find_wells_ [19]
[20]     0.9    1.34    0.30    9370         extreme_point_ [20]
                0.30    0.00  136183/346509      calcb_ [24]
-----------------------------------------------
                0.02    1.15      22/22          read_bfield_ [17]
[21]     0.6    0.02    1.15      22         jacobian_and_3dgrid_ [21]
                1.14    0.00      22/22          find_3dpoints_ [22]
                0.01    0.00      66/66          fill_3dgrid_ [41]
                0.00    0.00      22/22          check_jacsign_ [54]
-----------------------------------------------
                1.14    0.00      22/22          jacobian_and_3dgrid_ [21]
[22]     0.6    1.14    0.00      22         find_3dpoints_ [22]
                0.00    0.00      22/202488      calculate_time_ [42]
-----------------------------------------------
                0.00    0.80      22/22          MAIN__ [1]
[23]     0.4    0.00    0.80      22         calc_database_ [23]
                0.00    0.79      22/4997        calc_low_collisionality_ [5]
                0.01    0.00      22/7494        calc_ps_ [16]
                0.00    0.00      22/22910       calc_plateau_ [15]
                0.00    0.00      22/22          read_dkes_table_ [57]
                0.00    0.00      22/2454        dke_constants_ [51]
-----------------------------------------------
                0.00    0.00      60/346509      find_wells_ [19]
                0.00    0.00      60/346509      create_lambda_grid_ [40]
                0.30    0.00  136183/346509      extreme_point_ [20]
                0.47    0.00  210206/346509      bounce_point_ [14]
[24]     0.4    0.78    0.00  346509         calcb_ [24]
-----------------------------------------------
                0.62    0.00      30/30          calc_low_collisionality_nanl_ [6]
[25]     0.3    0.62    0.00      30         find_alpha_neighbours_ [25]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                0.08    0.38      44/44          read_bfield_ [17]
[26]     0.2    0.08    0.38      44         fill_bgrid_ [26]
                0.38    0.00      44/44          calc_absnablapsi_ [28]
-----------------------------------------------
                0.42    0.00      60/60          calc_low_collisionality_nanl_ [6]
[27]     0.2    0.42    0.00      60         label_gridpoints_ [27]
                0.00    0.00      60/202488      calculate_time_ [42]
-----------------------------------------------
                0.38    0.00      44/44          fill_bgrid_ [26]
[28]     0.2    0.38    0.00      44         calc_absnablapsi_ [28]
-----------------------------------------------
                0.06    0.00   30720/66912       integrate_g_ [11]
                0.07    0.00   36192/66912       create_angular_grid_ [31]
[29]     0.1    0.13    0.00   66912         fill_bnode_ [29]
-----------------------------------------------
                0.13    0.00     201/201         calc_low_collisionality_nanl_ [6]
[30]     0.1    0.13    0.00     201         sort_alpha_ [30]
-----------------------------------------------
                0.00    0.07      30/30          calc_low_collisionality_nanl_ [6]
[31]     0.0    0.00    0.07      30         create_angular_grid_ [31]
                0.07    0.00   36192/66912       fill_bnode_ [29]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                                                 <spontaneous>
[32]     0.0    0.07    0.00                 bounce_integrand_minf_ [32]
-----------------------------------------------
                0.06    0.00  105118/105118      integrate_g_ [11]
[33]     0.0    0.06    0.00  105118         fsa_ [33]
-----------------------------------------------
                0.02    0.02      66/66          read_bfield_ [17]
[34]     0.0    0.02    0.02      66         interpolate_field_ [34]
                0.01    0.00       3/3           find_bmin_bmax_ [44]
                0.01    0.00      66/88          fill_nm_ [43]
                0.00    0.00      66/202488      calculate_time_ [42]
-----------------------------------------------
                0.03    0.00    4997/4997        calc_low_collisionality_nanl_ [6]
[35]     0.0    0.03    0.00    4997         invert_matrix_petsc_ [35]
                0.00    0.00    9994/202488      calculate_time_ [42]
-----------------------------------------------
                0.02    0.00 1655209/1655209     characterize_wells_ [18]
[36]     0.0    0.02    0.00 1655209         match_wells_ [36]
-----------------------------------------------
                0.02    0.00      22/22          read_bfield_ [17]
[37]     0.0    0.02    0.00      22         read_boozmnnc_ [37]
-----------------------------------------------
                                                 <spontaneous>
[38]     0.0    0.02    0.00                 trilagrange_ [38]
-----------------------------------------------
                0.01    0.00    4997/4997        calc_low_collisionality_nanl_ [6]
[39]     0.0    0.01    0.00    4997         fill_matrix_petsc_ [39]
                0.00    0.00    4997/202488      calculate_time_ [42]
-----------------------------------------------
                0.01    0.00      30/30          calc_low_collisionality_nanl_ [6]
[40]     0.0    0.01    0.00      30         create_lambda_grid_ [40]
                0.00    0.00      60/346509      calcb_ [24]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                0.01    0.00      66/66          jacobian_and_3dgrid_ [21]
[41]     0.0    0.01    0.00      66         fill_3dgrid_ [41]
                0.00    0.00      66/202488      calculate_time_ [42]
-----------------------------------------------
                0.00    0.00       1/202488      MAIN__ [1]
                0.00    0.00      22/202488      solve_dke_qn_amb_ [3]
                0.00    0.00      22/202488      read_bfield_ [17]
                0.00    0.00      22/202488      find_3dpoints_ [22]
                0.00    0.00      30/202488      find_wells_ [19]
                0.00    0.00      30/202488      characterize_wells_ [18]
                0.00    0.00      30/202488      create_angular_grid_ [31]
                0.00    0.00      30/202488      exclude_wells_ [48]
                0.00    0.00      30/202488      create_lambda_grid_ [40]
                0.00    0.00      30/202488      coefficients_dke_ [7]
                0.00    0.00      30/202488      find_alpha_neighbours_ [25]
                0.00    0.00      30/202488      init_linear_problem_ [49]
                0.00    0.00      60/202488      label_gridpoints_ [27]
                0.00    0.00      66/202488      interpolate_field_ [34]
                0.00    0.00      66/202488      fill_3dgrid_ [41]
                0.00    0.00    4997/202488      calc_low_collisionality_nanl_ [6]
                0.00    0.00    4997/202488      fill_matrix_petsc_ [39]
                0.00    0.00    4997/202488      integrate_g_ [11]
                0.00    0.00    9994/202488      invert_matrix_petsc_ [35]
                0.00    0.00   71886/202488      calc_fluxes_ [4]
                0.01    0.00  105118/202488      bounces_ [8]
[42]     0.0    0.01    0.00  202488         calculate_time_ [42]
-----------------------------------------------
                0.00    0.00      22/88          read_bfield_ [17]
                0.01    0.00      66/88          interpolate_field_ [34]
[43]     0.0    0.01    0.00      88         fill_nm_ [43]
-----------------------------------------------
                0.01    0.00       3/3           interpolate_field_ [34]
[44]     0.0    0.01    0.00       3         find_bmin_bmax_ [44]
-----------------------------------------------
                                                 <spontaneous>
[45]     0.0    0.01    0.00                 ana_integral_ [45]
-----------------------------------------------
                0.00    0.00      66/66          read_plasmas_ [47]
[46]     0.0    0.00    0.00      66         read_profile_ [46]
                0.00    0.00     132/107640964     lagrange_ [13]
-----------------------------------------------
                0.00    0.00      22/22          MAIN__ [1]
[47]     0.0    0.00    0.00      22         read_plasmas_ [47]
                0.00    0.00      66/66          read_profile_ [46]
                0.00    0.00     132/132         rgauss_ [53]
-----------------------------------------------
                0.00    0.00      30/30          calc_low_collisionality_nanl_ [6]
[48]     0.0    0.00    0.00      30         exclude_wells_ [48]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                0.00    0.00      30/30          calc_low_collisionality_nanl_ [6]
[49]     0.0    0.00    0.00      30         init_linear_problem_ [49]
                0.00    0.00      30/202488      calculate_time_ [42]
-----------------------------------------------
                0.00    0.00  210176/210176      calc_low_collisionality_nanl_ [6]
[50]     0.0    0.00    0.00  210176         fill_dke_row_ [50]
-----------------------------------------------
                0.00    0.00      22/2454        calc_database_ [23]
                0.00    0.00    2432/2454        calc_fluxes_ [4]
[51]     0.0    0.00    0.00    2454         dke_constants_ [51]
-----------------------------------------------
                0.00    0.00    1216/1216        calc_fluxes_ [4]
[52]     0.0    0.00    0.00    1216         plot_flux_ [52]
-----------------------------------------------
                0.00    0.00     132/132         read_plasmas_ [47]
[53]     0.0    0.00    0.00     132         rgauss_ [53]
-----------------------------------------------
                0.00    0.00      22/22          jacobian_and_3dgrid_ [21]
[54]     0.0    0.00    0.00      22         check_jacsign_ [54]
-----------------------------------------------
                0.00    0.00      22/22          read_bfield_ [17]
[55]     0.0    0.00    0.00      22         read_addddkesdata_ [55]
-----------------------------------------------
                0.00    0.00      22/22          read_bfield_ [17]
[56]     0.0    0.00    0.00      22         read_boozmndata_ [56]
-----------------------------------------------
                0.00    0.00      22/22          calc_database_ [23]
[57]     0.0    0.00    0.00      22         read_dkes_table_ [57]
-----------------------------------------------
                0.00    0.00      22/22          MAIN__ [1]
[58]     0.0    0.00    0.00      22         read_sources_ [58]
-----------------------------------------------
                0.00    0.00      22/22          read_bfield_ [17]
[59]     0.0    0.00    0.00      22         rescale_ [59]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[60]     0.0    0.00    0.00       1         distribute_mpi_ [60]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[61]     0.0    0.00    0.00       1         end_all_ [61]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[62]     0.0    0.00    0.00       1         init_files_ [62]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[63]     0.0    0.00    0.00       1         init_randomseed_ [63]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[64]     0.0    0.00    0.00       1         initialize_mpi_ [64]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[65]     0.0    0.00    0.00       1         read_input_ [65]
-----------------------------------------------
                0.00    0.00       1/1           MAIN__ [1]
[66]     0.0    0.00    0.00       1         transport_ [66]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2017 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

   [1] MAIN__                 [60] distribute_mpi_        [35] invert_matrix_petsc_
  [45] ana_integral_          [51] dke_constants_         [21] jacobian_and_3dgrid_
   [9] bounce_integral_       [61] end_all_               [27] label_gridpoints_
  [10] bounce_integrand_      [48] exclude_wells_         [13] lagrange_
  [32] bounce_integrand_minf_ [20] extreme_point_         [36] match_wells_
  [14] bounce_point_          [41] fill_3dgrid_           [52] plot_flux_
   [8] bounces_               [26] fill_bgrid_            [55] read_addddkesdata_
  [28] calc_absnablapsi_      [29] fill_bnode_            [17] read_bfield_
  [23] calc_database_         [50] fill_dke_row_          [56] read_boozmndata_
   [4] calc_fluxes_           [39] fill_matrix_petsc_     [37] read_boozmnnc_
   [5] calc_low_collisionality_ [43] fill_nm_             [57] read_dkes_table_
   [6] calc_low_collisionality_nanl_ [22] find_3dpoints_  [65] read_input_
  [15] calc_plateau_          [25] find_alpha_neighbours_ [47] read_plasmas_
  [16] calc_ps_               [44] find_bmin_bmax_        [46] read_profile_
  [24] calcb_                 [19] find_wells_            [58] read_sources_
  [42] calculate_time_        [33] fsa_                   [59] rescale_
  [18] characterize_wells_    [62] init_files_            [53] rgauss_
  [54] check_jacsign_         [49] init_linear_problem_    [3] solve_dke_qn_amb_
   [7] coefficients_dke_      [63] init_randomseed_       [30] sort_alpha_
  [31] create_angular_grid_   [64] initialize_mpi_        [66] transport_
  [40] create_lambda_grid_    [11] integrate_g_           [38] trilagrange_
  [12] delta_phase_           [34] interpolate_field_
