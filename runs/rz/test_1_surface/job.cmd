#!/bin/bash
#SBATCH --job-name=knosos
#SBATCH --output=%j.out
#SBATCH --error=%j.err 
#SBATCH --ntasks=1 

source /gpfs/projects/bsc99/bsc99204/KNOSOS/compilation_env.sh

time srun knosos.x 
