#!/bin/bash
#SBATCH --job-name=knosos
#SBATCH --output=%j.out
#SBATCH --error=%j.err 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --exclusive

source /gpfs/projects/bsc99/bsc99204/KNOSOS/compilation_env_nord3.sh

time srun knosos.x 
