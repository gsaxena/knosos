#!/bin/bash
#SBATCH --job-name=knosos
#SBATCH --output=%j.out
#SBATCH --error=%j.err 
#SBATCH --ntasks=1 
#SBATCH --constraint=perfparanoid

source /gpfs/projects/bsc99/bsc99204/KNOSOS/compilation_env.sh
#module load valgrind

srun valgrind --tool=callgrind ./knosos.x 
