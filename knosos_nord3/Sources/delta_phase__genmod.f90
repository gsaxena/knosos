        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:18 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE DELTA_PHASE__genmod
          INTERFACE 
            SUBROUTINE DELTA_PHASE(COSNM,SINNM,COSNM_DEL,SINNM_DEL)
              USE GLOBAL
              REAL(KIND=8) :: COSNM(NNM)
              REAL(KIND=8) :: SINNM(NNM)
              REAL(KIND=8) :: COSNM_DEL(NNM)
              REAL(KIND=8) :: SINNM_DEL(NNM)
            END SUBROUTINE DELTA_PHASE
          END INTERFACE 
        END MODULE DELTA_PHASE__genmod
