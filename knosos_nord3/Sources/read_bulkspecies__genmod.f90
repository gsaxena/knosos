        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE READ_BULKSPECIES__genmod
          INTERFACE 
            SUBROUTINE READ_BULKSPECIES(NALPHAB,FILENAME,Q,FACT)
              INTEGER(KIND=4) :: NALPHAB
              CHARACTER(LEN=3) :: FILENAME
              REAL(KIND=8) :: Q(NALPHAB,NALPHAB)
              REAL(KIND=8) :: FACT
            END SUBROUTINE READ_BULKSPECIES
          END INTERFACE 
        END MODULE READ_BULKSPECIES__genmod
