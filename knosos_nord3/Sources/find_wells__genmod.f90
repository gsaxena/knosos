        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:18 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FIND_WELLS__genmod
          INTERFACE 
            SUBROUTINE FIND_WELLS(NA,MTURN,NW0,Z1,T1,B1,HBPP1,VD1,ZB,TB,&
     &BB,HBPPB,VDB,Z2,T2,B2,HBPP2,VD2,NW,ALPHAP,OFFSET)
              USE GLOBAL
              INTEGER(KIND=4) :: NA
              INTEGER(KIND=4) :: MTURN
              INTEGER(KIND=4) :: NW0
              REAL(KIND=8) :: Z1(5000)
              REAL(KIND=8) :: T1(5000)
              REAL(KIND=8) :: B1(5000)
              REAL(KIND=8) :: HBPP1(5000)
              REAL(KIND=8) :: VD1(3,5000)
              REAL(KIND=8) :: ZB(5000)
              REAL(KIND=8) :: TB(5000)
              REAL(KIND=8) :: BB(5000)
              REAL(KIND=8) :: HBPPB(5000)
              REAL(KIND=8) :: VDB(3,5000)
              REAL(KIND=8) :: Z2(5000)
              REAL(KIND=8) :: T2(5000)
              REAL(KIND=8) :: B2(5000)
              REAL(KIND=8) :: HBPP2(5000)
              REAL(KIND=8) :: VD2(3,5000)
              INTEGER(KIND=4) :: NW
              REAL(KIND=8) :: ALPHAP(5000)
              REAL(KIND=8) :: OFFSET
            END SUBROUTINE FIND_WELLS
          END INTERFACE 
        END MODULE FIND_WELLS__genmod
