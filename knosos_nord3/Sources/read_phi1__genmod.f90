        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE READ_PHI1__genmod
          INTERFACE 
            SUBROUTINE READ_PHI1(NALPHAB,PHI1C)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: PHI1C(NNMP)
            END SUBROUTINE READ_PHI1
          END INTERFACE 
        END MODULE READ_PHI1__genmod
