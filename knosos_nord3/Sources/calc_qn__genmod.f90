        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_QN__genmod
          INTERFACE 
            SUBROUTINE CALC_QN(JT,JT0,PHI1ANM,PHI1NM,PHI1C)
              USE GLOBAL
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: JT0
              REAL(KIND=8) :: PHI1ANM(NNMP,NNMP)
              REAL(KIND=8) :: PHI1NM(NNMP,NNMP)
              REAL(KIND=8) :: PHI1C(100,NNMP)
            END SUBROUTINE CALC_QN
          END INTERFACE 
        END MODULE CALC_QN__genmod
