        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:18 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_VDBP__genmod
          INTERFACE 
            SUBROUTINE CALC_VDBP(Z_IN,T_IN,B_OUT,BP_OUT,HBPP_OUT,VD)
              USE GLOBAL
              REAL(KIND=8) :: Z_IN
              REAL(KIND=8) :: T_IN
              REAL(KIND=8) :: B_OUT
              REAL(KIND=8) :: BP_OUT
              REAL(KIND=8) :: HBPP_OUT
              REAL(KIND=8) :: VD(3)
            END SUBROUTINE CALC_VDBP
          END INTERFACE 
        END MODULE CALC_VDBP__genmod
