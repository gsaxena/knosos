        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:18 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE BOUNCES__genmod
          INTERFACE 
            SUBROUTINE BOUNCES(IW,Z1X,T1X,B1X,HBPP1X,VD1X,ZBX,TBX,BBX,  &
     &HBPPBX,VDBX,Z2X,T2X,B2X,HBPP2X,VD2X,BBOUNCE,TOP,NQ,Q,Z1,T1,Z2,T2)
              INTEGER(KIND=4) :: NQ
              INTEGER(KIND=4) :: IW
              REAL(KIND=8) :: Z1X
              REAL(KIND=8) :: T1X
              REAL(KIND=8) :: B1X
              REAL(KIND=8) :: HBPP1X
              REAL(KIND=8) :: VD1X(3)
              REAL(KIND=8) :: ZBX
              REAL(KIND=8) :: TBX
              REAL(KIND=8) :: BBX
              REAL(KIND=8) :: HBPPBX
              REAL(KIND=8) :: VDBX(3)
              REAL(KIND=8) :: Z2X
              REAL(KIND=8) :: T2X
              REAL(KIND=8) :: B2X
              REAL(KIND=8) :: HBPP2X
              REAL(KIND=8) :: VD2X(3)
              REAL(KIND=8) :: BBOUNCE
              LOGICAL(KIND=4) :: TOP
              REAL(KIND=8) :: Q(NQ)
              REAL(KIND=8) :: Z1
              REAL(KIND=8) :: T1
              REAL(KIND=8) :: Z2
              REAL(KIND=8) :: T2
            END SUBROUTINE BOUNCES
          END INTERFACE 
        END MODULE BOUNCES__genmod
