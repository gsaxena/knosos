        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:18 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_PHASE__genmod
          INTERFACE 
            SUBROUTINE FILL_PHASE(Z_L,T_L,COSNM,SINNM)
              USE GLOBAL
              REAL(KIND=8) :: Z_L
              REAL(KIND=8) :: T_L
              REAL(KIND=8) :: COSNM(NNM)
              REAL(KIND=8) :: SINNM(NNM)
            END SUBROUTINE FILL_PHASE
          END INTERFACE 
        END MODULE FILL_PHASE__genmod
