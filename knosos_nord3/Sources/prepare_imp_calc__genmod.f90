        !COMPILER-GENERATED INTERFACE MODULE: Thu Nov 10 11:27:10 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE PREPARE_IMP_CALC__genmod
          INTERFACE 
            SUBROUTINE PREPARE_IMP_CALC(JT,JT0,NBB,NALPHAB,TRIG,DTRIGDZ,&
     &DTRIGDT,N1NMB,MBBNM,TRMNM,PHI1C,AI,TI,EPSI,S,ZETA,THETA,PHI1,MBB, &
     &TRM)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: JT0
              REAL(KIND=8) :: TRIG(NNMP,NALPHAB*NALPHAB)
              REAL(KIND=8) :: DTRIGDZ(NNMP,NALPHAB*NALPHAB)
              REAL(KIND=8) :: DTRIGDT(NNMP,NALPHAB*NALPHAB)
              REAL(KIND=8) :: N1NMB(NNMP,NBB)
              REAL(KIND=8) :: MBBNM(NNMP)
              REAL(KIND=8) :: TRMNM(NNMP)
              REAL(KIND=8) :: PHI1C(NNMP)
              REAL(KIND=8) :: AI
              REAL(KIND=8) :: TI
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: S
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: PHI1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: MBB(NALPHAB,NALPHAB)
              REAL(KIND=8) :: TRM(NALPHAB,NALPHAB)
            END SUBROUTINE PREPARE_IMP_CALC
          END INTERFACE 
        END MODULE PREPARE_IMP_CALC__genmod
