        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:28 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE READ_INPUT__genmod
          INTERFACE 
            SUBROUTINE READ_INPUT(DT,NS,S,NBB,ZB,AB,REGB,FRACB)
              REAL(KIND=8) :: DT
              INTEGER(KIND=4) :: NS
              REAL(KIND=8) :: S(100)
              INTEGER(KIND=4) :: NBB
              REAL(KIND=8) :: ZB(11)
              REAL(KIND=8) :: AB(11)
              INTEGER(KIND=4) :: REGB(11)
              REAL(KIND=8) :: FRACB(11)
            END SUBROUTINE READ_INPUT
          END INTERFACE 
        END MODULE READ_INPUT__genmod
