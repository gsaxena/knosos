        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:52 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE TRANSPORT__genmod
          INTERFACE 
            SUBROUTINE TRANSPORT(DT,NBB,NS,S,ZB,AB,REGB,NB,DNBDPSI,GB,SB&
     &,TB,DTBDPSI,QB,PB,EPSI)
              INTEGER(KIND=4) :: NS
              INTEGER(KIND=4) :: NBB
              REAL(KIND=8) :: DT
              REAL(KIND=8) :: S(NS)
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              INTEGER(KIND=4) :: REGB(NBB)
              REAL(KIND=8) :: NB(NBB,NS)
              REAL(KIND=8) :: DNBDPSI(NBB,NS)
              REAL(KIND=8) :: GB(NBB,NS)
              REAL(KIND=8) :: SB(NBB,NS)
              REAL(KIND=8) :: TB(NBB,NS)
              REAL(KIND=8) :: DTBDPSI(NBB,NS)
              REAL(KIND=8) :: QB(NBB,NS)
              REAL(KIND=8) :: PB(NBB,NS)
              REAL(KIND=8) :: EPSI(NS)
            END SUBROUTINE TRANSPORT
          END INTERFACE 
        END MODULE TRANSPORT__genmod
