        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FIND_ALPHA_NEIGHBOURS__genmod
          INTERFACE 
            SUBROUTINE FIND_ALPHA_NEIGHBOURS(NPOINT,I_P,I_W,I_L,I_P_LM1,&
     &I_P_LP1,NBIFX,NBIF,ZLW,ZRW,NW,CONNECTED,BI6,NLAMBDA,NALPHA,NALPHAB&
     &,ALPHAP,I_P_AM1,I_P_AP1,DALPHA_AM1,DALPHA_AP1,I_P_AM2,I_P_AP2,    &
     &DALPHA_AM2,DALPHA_AP2,I_P_AM1I,I_P_AM1II,I_P_AM2I,I_P_AM2II,      &
     &I_P_AM2III,I_P_AM2IV,I_P_AP1I,I_P_AP1II,I_P_AP2I,I_P_AP2II,       &
     &I_P_AP2III,I_P_AP2IV,WM1I,WM1II,WM2I,WM2II,WM2III,WM2IV,WP1I,WP1II&
     &,WP2I,WP2II,WP2III,WP2IV,LAMBDA,BI7,BI3,BI3B,BI3F,DLAMBDA_LM1,    &
     &DLAMBDA_LP1)
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NW
              INTEGER(KIND=4) :: NBIFX
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
              INTEGER(KIND=4) :: I_W(NPOINT)
              INTEGER(KIND=4) :: I_L(NPOINT)
              INTEGER(KIND=4) :: I_P_LM1(NPOINT)
              INTEGER(KIND=4) :: I_P_LP1(NBIFX,NPOINT)
              INTEGER(KIND=4) :: NBIF(NPOINT)
              REAL(KIND=8) :: ZLW(NPOINT)
              REAL(KIND=8) :: ZRW(NPOINT)
              LOGICAL(KIND=4) :: CONNECTED(NW,NW)
              REAL(KIND=8) :: BI6(NPOINT)
              REAL(KIND=8) :: ALPHAP(NALPHA)
              INTEGER(KIND=4) :: I_P_AM1(NPOINT)
              INTEGER(KIND=4) :: I_P_AP1(NPOINT)
              REAL(KIND=8) :: DALPHA_AM1(NPOINT)
              REAL(KIND=8) :: DALPHA_AP1(NPOINT)
              INTEGER(KIND=4) :: I_P_AM2(NPOINT)
              INTEGER(KIND=4) :: I_P_AP2(NPOINT)
              REAL(KIND=8) :: DALPHA_AM2(NPOINT)
              REAL(KIND=8) :: DALPHA_AP2(NPOINT)
              INTEGER(KIND=4) :: I_P_AM1I(NPOINT)
              INTEGER(KIND=4) :: I_P_AM1II(NPOINT)
              INTEGER(KIND=4) :: I_P_AM2I(NPOINT)
              INTEGER(KIND=4) :: I_P_AM2II(NPOINT)
              INTEGER(KIND=4) :: I_P_AM2III(NPOINT)
              INTEGER(KIND=4) :: I_P_AM2IV(NPOINT)
              INTEGER(KIND=4) :: I_P_AP1I(NPOINT)
              INTEGER(KIND=4) :: I_P_AP1II(NPOINT)
              INTEGER(KIND=4) :: I_P_AP2I(NPOINT)
              INTEGER(KIND=4) :: I_P_AP2II(NPOINT)
              INTEGER(KIND=4) :: I_P_AP2III(NPOINT)
              INTEGER(KIND=4) :: I_P_AP2IV(NPOINT)
              REAL(KIND=8) :: WM1I(NPOINT)
              REAL(KIND=8) :: WM1II(NPOINT)
              REAL(KIND=8) :: WM2I(NPOINT)
              REAL(KIND=8) :: WM2II(NPOINT)
              REAL(KIND=8) :: WM2III(NPOINT)
              REAL(KIND=8) :: WM2IV(NPOINT)
              REAL(KIND=8) :: WP1I(NPOINT)
              REAL(KIND=8) :: WP1II(NPOINT)
              REAL(KIND=8) :: WP2I(NPOINT)
              REAL(KIND=8) :: WP2II(NPOINT)
              REAL(KIND=8) :: WP2III(NPOINT)
              REAL(KIND=8) :: WP2IV(NPOINT)
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              REAL(KIND=8) :: BI7(NPOINT)
              REAL(KIND=8) :: BI3(NPOINT)
              REAL(KIND=8) :: BI3B(NPOINT)
              REAL(KIND=8) :: BI3F(NPOINT)
              REAL(KIND=8) :: DLAMBDA_LM1(NPOINT)
              REAL(KIND=8) :: DLAMBDA_LP1(NPOINT)
            END SUBROUTINE FIND_ALPHA_NEIGHBOURS
          END INTERFACE 
        END MODULE FIND_ALPHA_NEIGHBOURS__genmod
