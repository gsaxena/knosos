        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INTEGRATE_G__genmod
          INTERFACE 
            SUBROUTINE INTEGRATE_G(NALPHA,NALPHAB,NLAMBDA,LAMBDA,I_P,   &
     &NPOINT,G,NOTG,ZETAP,THETAP,THETA,B_AL,VDS_AL,D11,DN1,DN1NM)
              USE GLOBAL
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
              REAL(KIND=8) :: G(NPOINT,NNMP)
              LOGICAL(KIND=4) :: NOTG
              REAL(KIND=8) :: ZETAP(NALPHAB)
              REAL(KIND=8) :: THETAP(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: VDS_AL(NNMP,NALPHA,NALPHAB)
              REAL(KIND=8) :: D11(NNMP,NNMP)
              REAL(KIND=8) :: DN1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DN1NM(NNMP,NNMP)
            END SUBROUTINE INTEGRATE_G
          END INTERFACE 
        END MODULE INTEGRATE_G__genmod
