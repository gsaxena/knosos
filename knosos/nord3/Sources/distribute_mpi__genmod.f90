        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:44 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE DISTRIBUTE_MPI__genmod
          INTERFACE 
            SUBROUTINE DISTRIBUTE_MPI(NS,RANK)
              USE GLOBAL
              INTEGER(KIND=4) :: NS
              INTEGER(KIND=4) :: RANK(NS,NERR)
            END SUBROUTINE DISTRIBUTE_MPI
          END INTERFACE 
        END MODULE DISTRIBUTE_MPI__genmod
