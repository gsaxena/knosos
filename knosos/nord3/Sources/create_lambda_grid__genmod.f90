        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CREATE_LAMBDA_GRID__genmod
          INTERFACE 
            SUBROUTINE CREATE_LAMBDA_GRID(NLAMBDA,NW,BB,BT,LAMBDAB_W,   &
     &LAMBDAC_W,LAMBDAC,DLAMBDAP,LAMBDA,ONE_O_LAMBDA)
              USE GLOBAL
              INTEGER(KIND=4) :: NW
              INTEGER(KIND=4) :: NLAMBDA
              REAL(KIND=8) :: BB(NW)
              REAL(KIND=8) :: BT(NW)
              REAL(KIND=8) :: LAMBDAB_W(NW)
              REAL(KIND=8) :: LAMBDAC_W(NW)
              REAL(KIND=8) :: LAMBDAC
              REAL(KIND=8) :: DLAMBDAP
              REAL(KIND=8) :: LAMBDA(1024)
              REAL(KIND=8) :: ONE_O_LAMBDA(1024)
            END SUBROUTINE CREATE_LAMBDA_GRID
          END INTERFACE 
        END MODULE CREATE_LAMBDA_GRID__genmod
