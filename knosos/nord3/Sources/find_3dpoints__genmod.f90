        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:14 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FIND_3DPOINTS__genmod
          INTERFACE 
            SUBROUTINE FIND_3DPOINTS(NZ,NT,S,X1,X2,X3)
              INTEGER(KIND=4) :: NT
              INTEGER(KIND=4) :: NZ
              REAL(KIND=8) :: S
              REAL(KIND=8) :: X1(NZ,NT)
              REAL(KIND=8) :: X2(NZ,NT)
              REAL(KIND=8) :: X3(NZ,NT)
            END SUBROUTINE FIND_3DPOINTS
          END INTERFACE 
        END MODULE FIND_3DPOINTS__genmod
