        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_MONOENERGETIC__genmod
          INTERFACE 
            SUBROUTINE CALC_MONOENERGETIC(IB,ZB,AB,REGB,REGP,JT,IV,EPSI,&
     &PHI1C,MBBNM,TRMNM,D11,NALPHAB,ZETA,THETA,DN1NMDV,D31)
              USE GLOBAL
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: ZB
              REAL(KIND=8) :: AB
              INTEGER(KIND=4) :: REGB
              LOGICAL(KIND=4) :: REGP
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: IV
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: PHI1C(NNMP)
              REAL(KIND=8) :: MBBNM(NNMP)
              REAL(KIND=8) :: TRMNM(NNMP)
              REAL(KIND=8) :: D11(NNMP,NNMP)
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(512)
              REAL(KIND=8) :: THETA(512)
              REAL(KIND=8) :: DN1NMDV(NNMP,NNMP)
              REAL(KIND=8) :: D31
            END SUBROUTINE CALC_MONOENERGETIC
          END INTERFACE 
        END MODULE CALC_MONOENERGETIC__genmod
