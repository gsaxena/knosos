        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INVERT_QN__genmod
          INTERFACE 
            SUBROUTINE INVERT_QN(NROW,MAT,RHS)
              INTEGER(KIND=4) :: NROW
              REAL(KIND=8) :: MAT(NROW,NROW)
              REAL(KIND=8) :: RHS(NROW)
            END SUBROUTINE INVERT_QN
          END INTERFACE 
        END MODULE INVERT_QN__genmod
