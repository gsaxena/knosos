        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE EXCLUDE_WELLS__genmod
          INTERFACE 
            SUBROUTINE EXCLUDE_WELLS(NA,NALPHA,NALPHAB,NW,BOTTOM,       &
     &CONNECTED,ALPHAP_W,Z1,ZB,Z2,BB,BT,ZETAX,THETAX)
              INTEGER(KIND=4) :: NW
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              INTEGER(KIND=4) :: NA
              LOGICAL(KIND=4) :: BOTTOM(NW)
              LOGICAL(KIND=4) :: CONNECTED(NW,NW)
              REAL(KIND=8) :: ALPHAP_W(NW)
              REAL(KIND=8) :: Z1(NW)
              REAL(KIND=8) :: ZB(NW)
              REAL(KIND=8) :: Z2(NW)
              REAL(KIND=8) :: BB(NW)
              REAL(KIND=8) :: BT(NW)
              REAL(KIND=8) :: ZETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETAX(NALPHA,NALPHAB)
            END SUBROUTINE EXCLUDE_WELLS
          END INTERFACE 
        END MODULE EXCLUDE_WELLS__genmod
