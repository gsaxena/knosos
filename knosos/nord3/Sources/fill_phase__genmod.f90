        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:13 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FILL_PHASE__genmod
          INTERFACE 
            SUBROUTINE FILL_PHASE(Z_L,T_L,COSNM,SINNM)
              USE GLOBAL
              REAL(KIND=8) :: Z_L
              REAL(KIND=8) :: T_L
              REAL(KIND=8) :: COSNM(NNM)
              REAL(KIND=8) :: SINNM(NNM)
            END SUBROUTINE FILL_PHASE
          END INTERFACE 
        END MODULE FILL_PHASE__genmod
