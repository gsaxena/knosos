        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:49 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALCULATE_U__genmod
          INTERFACE 
            SUBROUTINE CALCULATE_U(NALPHAB,ZETA,THETA,B,DBDZ,DBDT,ZE_T, &
     &PHI1,DPHI1DZ,DPHI1DT,EXPMZEPOT,U0,U1,U2)
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DBDZ(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DBDT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: ZE_T
              REAL(KIND=8) :: PHI1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DPHI1DZ(NALPHAB,NALPHAB)
              REAL(KIND=8) :: DPHI1DT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: EXPMZEPOT(NALPHAB,NALPHAB)
              REAL(KIND=8) :: U0(NALPHAB,NALPHAB)
              REAL(KIND=8) :: U1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: U2(NALPHAB,NALPHAB)
            END SUBROUTINE CALCULATE_U
          END INTERFACE 
        END MODULE CALCULATE_U__genmod
