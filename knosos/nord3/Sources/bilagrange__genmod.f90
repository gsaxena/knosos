        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:29 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE BILAGRANGE__genmod
          INTERFACE 
            SUBROUTINE BILAGRANGE(X1A,X2A,YA,M,N,X1,X2,Y,ORDER)
              INTEGER(KIND=4) :: N
              INTEGER(KIND=4) :: M
              REAL(KIND=8) :: X1A(M)
              REAL(KIND=8) :: X2A(N)
              REAL(KIND=8) :: YA(M,N)
              REAL(KIND=8) :: X1
              REAL(KIND=8) :: X2
              REAL(KIND=8) :: Y
              INTEGER(KIND=4) :: ORDER
            END SUBROUTINE BILAGRANGE
          END INTERFACE 
        END MODULE BILAGRANGE__genmod
