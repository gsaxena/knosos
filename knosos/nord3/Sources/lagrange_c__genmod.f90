        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:29 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE LAGRANGE_C__genmod
          INTERFACE 
            SUBROUTINE LAGRANGE_C(XA,YA,N,X,Y,ORDER)
              INTEGER(KIND=4) :: N
              REAL(KIND=8) :: XA(N)
              COMPLEX(KIND=8) :: YA(N)
              REAL(KIND=8) :: X
              COMPLEX(KIND=8) :: Y
              INTEGER(KIND=4) :: ORDER
            END SUBROUTINE LAGRANGE_C
          END INTERFACE 
        END MODULE LAGRANGE_C__genmod
