        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INTEGRATE_V__genmod
          INTERFACE 
            SUBROUTINE INTEGRATE_V(JT,IB,AB,REGP,TB,IV,D11,DN1NMDV,L1RHS&
     &,L2RHS,L3RHS,GRHS,QRHS,N1NMRHS,MBBNMRHS,TRMNMRHS,D31,L31,L32)
              USE GLOBAL
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: AB
              LOGICAL(KIND=4) :: REGP
              REAL(KIND=8) :: TB
              INTEGER(KIND=4) :: IV
              REAL(KIND=8) :: D11(NNMP,NNMP)
              REAL(KIND=8) :: DN1NMDV(NNMP,NNMP)
              REAL(KIND=8) :: L1RHS(NNMP,NNMP)
              REAL(KIND=8) :: L2RHS(NNMP,NNMP)
              REAL(KIND=8) :: L3RHS(NNMP,NNMP)
              REAL(KIND=8) :: GRHS(NNMP,NNMP)
              REAL(KIND=8) :: QRHS(NNMP,NNMP)
              REAL(KIND=8) :: N1NMRHS(NNMP,NNMP)
              REAL(KIND=8) :: MBBNMRHS(NNMP,NNMP)
              REAL(KIND=8) :: TRMNMRHS(NNMP,NNMP)
              REAL(KIND=8) :: D31
              REAL(KIND=8) :: L31
              REAL(KIND=8) :: L32
            END SUBROUTINE INTEGRATE_V
          END INTERFACE 
        END MODULE INTEGRATE_V__genmod
