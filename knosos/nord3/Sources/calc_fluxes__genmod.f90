        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_FLUXES__genmod
          INTERFACE 
            SUBROUTINE CALC_FLUXES(IT,NBB,ZB,AB,REGB,S,NB,DNBDPSI,TB,   &
     &DTBDPSI,EPSI,GB,QB,L1B,L2B,L3B,EPHI1OTSIZE)
              USE GLOBAL
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: IT
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              INTEGER(KIND=4) :: REGB(NBB)
              REAL(KIND=8) :: S
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: GB(NBB)
              REAL(KIND=8) :: QB(NBB)
              REAL(KIND=8) :: L1B(NBB)
              REAL(KIND=8) :: L2B(NBB)
              REAL(KIND=8) :: L3B(NBB)
              REAL(KIND=8) :: EPHI1OTSIZE
            END SUBROUTINE CALC_FLUXES
          END INTERFACE 
        END MODULE CALC_FLUXES__genmod
