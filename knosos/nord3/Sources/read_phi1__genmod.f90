        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE READ_PHI1__genmod
          INTERFACE 
            SUBROUTINE READ_PHI1(NALPHAB,PHI1C)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: PHI1C(NNMP)
            END SUBROUTINE READ_PHI1
          END INTERFACE 
        END MODULE READ_PHI1__genmod
