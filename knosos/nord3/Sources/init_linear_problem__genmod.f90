        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INIT_LINEAR_PROBLEM__genmod
          INTERFACE 
            SUBROUTINE INIT_LINEAR_PROBLEM(NPOINT,NNZ,MATCOL,MATVEAF,   &
     &MATVEAB,MATVMAF,MATVMAB,MATDIFF,MATDIFB)
              USE PETSCKSP
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: NNZ(NPOINT)
              TYPE (TMAT) :: MATCOL
              TYPE (TMAT) :: MATVEAF
              TYPE (TMAT) :: MATVEAB
              TYPE (TMAT) :: MATVMAF
              TYPE (TMAT) :: MATVMAB
              TYPE (TMAT) :: MATDIFF
              TYPE (TMAT) :: MATDIFB
            END SUBROUTINE INIT_LINEAR_PROBLEM
          END INTERFACE 
        END MODULE INIT_LINEAR_PROBLEM__genmod
