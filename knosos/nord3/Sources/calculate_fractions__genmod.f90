        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:22 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALCULATE_FRACTIONS__genmod
          INTERFACE 
            SUBROUTINE CALCULATE_FRACTIONS(VS,NALPHA,NALPHAB,NLAMBDA,   &
     &LAMBDA,I_P,NPOINT,THETAP,B_AL,VDS_AL,TAU,IA_OUT)
              USE GLOBAL
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              REAL(KIND=8) :: VS
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
              REAL(KIND=8) :: THETAP(NALPHA,NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              REAL(KIND=8) :: VDS_AL(NNMP,NALPHA,NALPHAB)
              REAL(KIND=8) :: TAU(NPOINT)
              INTEGER(KIND=4) :: IA_OUT(NPOINT)
            END SUBROUTINE CALCULATE_FRACTIONS
          END INTERFACE 
        END MODULE CALCULATE_FRACTIONS__genmod
