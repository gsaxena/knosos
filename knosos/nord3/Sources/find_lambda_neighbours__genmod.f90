        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE FIND_LAMBDA_NEIGHBOURS__genmod
          INTERFACE 
            SUBROUTINE FIND_LAMBDA_NEIGHBOURS(NPOINT,NALPHA,NALPHAB,    &
     &NLAMBDA,NW,NBIFX,I_P,BOTTOM,I_W,I_P_LM1,NBIF,I_P_LP1)
              INTEGER(KIND=4) :: NBIFX
              INTEGER(KIND=4) :: NW
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
              LOGICAL(KIND=4) :: BOTTOM(NW)
              INTEGER(KIND=4) :: I_W(NPOINT)
              INTEGER(KIND=4) :: I_P_LM1(NPOINT)
              INTEGER(KIND=4) :: NBIF(NPOINT)
              INTEGER(KIND=4) :: I_P_LP1(NBIFX,NPOINT)
            END SUBROUTINE FIND_LAMBDA_NEIGHBOURS
          END INTERFACE 
        END MODULE FIND_LAMBDA_NEIGHBOURS__genmod
