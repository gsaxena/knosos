        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:13 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALCB_DEL__genmod
          INTERFACE 
            SUBROUTINE CALCB_DEL(COSNM,SINNM,FLAG,FLAGB1,B_0,DBDZ_0,    &
     &DBDT_0,DBDPSI,B_1,DBDZ_1,DBDT_1,PHI_1,DPHDZ,DPHDT)
              USE GLOBAL
              REAL(KIND=8) :: COSNM(NNM)
              REAL(KIND=8) :: SINNM(NNM)
              INTEGER(KIND=4) :: FLAG
              LOGICAL(KIND=4) :: FLAGB1
              REAL(KIND=8) :: B_0
              REAL(KIND=8) :: DBDZ_0
              REAL(KIND=8) :: DBDT_0
              REAL(KIND=8) :: DBDPSI
              REAL(KIND=8) :: B_1
              REAL(KIND=8) :: DBDZ_1
              REAL(KIND=8) :: DBDT_1
              REAL(KIND=8) :: PHI_1
              REAL(KIND=8) :: DPHDZ
              REAL(KIND=8) :: DPHDT
            END SUBROUTINE CALCB_DEL
          END INTERFACE 
        END MODULE CALCB_DEL__genmod
