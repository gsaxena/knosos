        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE LABEL_GRIDPOINTS__genmod
          INTERFACE 
            SUBROUTINE LABEL_GRIDPOINTS(NALPHA,NALPHAB,NLAMBDA,NW,BOTTOM&
     &,CONNECTED,ALPHAP_W,Z1,Z2,BB,BT,LAMBDA,ZETAP,ZETAX,THETAX,B_AL,   &
     &NPOINT,I_L,I_W,I_P)
              INTEGER(KIND=4) :: NW
              INTEGER(KIND=4) :: NLAMBDA
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NALPHA
              LOGICAL(KIND=4) :: BOTTOM(NW)
              LOGICAL(KIND=4) :: CONNECTED(NW,NW)
              REAL(KIND=8) :: ALPHAP_W(NW)
              REAL(KIND=8) :: Z1(NW)
              REAL(KIND=8) :: Z2(NW)
              REAL(KIND=8) :: BB(NW)
              REAL(KIND=8) :: BT(NW)
              REAL(KIND=8) :: LAMBDA(NLAMBDA)
              REAL(KIND=8) :: ZETAP(NALPHAB)
              REAL(KIND=8) :: ZETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: THETAX(NALPHA,NALPHAB)
              REAL(KIND=8) :: B_AL(NALPHA,NALPHAB)
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: I_L(400000)
              INTEGER(KIND=4) :: I_W(400000)
              INTEGER(KIND=4) :: I_P(NLAMBDA,NALPHA,NALPHAB)
            END SUBROUTINE LABEL_GRIDPOINTS
          END INTERFACE 
        END MODULE LABEL_GRIDPOINTS__genmod
