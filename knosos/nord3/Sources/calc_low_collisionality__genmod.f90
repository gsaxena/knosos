        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_LOW_COLLISIONALITY__genmod
          INTERFACE 
            SUBROUTINE CALC_LOW_COLLISIONALITY(JV,EPSI,PHI1C,MBBNM,TRMNM&
     &,D11,NALPHAB,ZETA,THETA,DN1DV,DN1NM)
              USE GLOBAL
              INTEGER(KIND=4) :: JV
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: PHI1C(NNMP)
              REAL(KIND=8) :: MBBNM(NNMP)
              REAL(KIND=8) :: TRMNM(NNMP)
              REAL(KIND=8) :: D11(NNMP,NNMP)
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(512)
              REAL(KIND=8) :: THETA(512)
              REAL(KIND=8) :: DN1DV(512,512)
              REAL(KIND=8) :: DN1NM(NNMP,NNMP)
            END SUBROUTINE CALC_LOW_COLLISIONALITY
          END INTERFACE 
        END MODULE CALC_LOW_COLLISIONALITY__genmod
