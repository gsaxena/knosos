        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:49 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_ABSNABLAPSI__genmod
          INTERFACE 
            SUBROUTINE CALC_ABSNABLAPSI(NALPHAB,ZETA,THETA,             &
     &ABSNABLAPSI2OB2)
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: ABSNABLAPSI2OB2(NALPHAB,NALPHAB)
            END SUBROUTINE CALC_ABSNABLAPSI
          END INTERFACE 
        END MODULE CALC_ABSNABLAPSI__genmod
