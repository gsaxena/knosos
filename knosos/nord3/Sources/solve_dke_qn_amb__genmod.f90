        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE SOLVE_DKE_QN_AMB__genmod
          INTERFACE 
            SUBROUTINE SOLVE_DKE_QN_AMB(IT,NBB,ZB,AB,REGB,S,NB,DNBDPSI, &
     &TB,DTBDPSI,EPSI,GB,QB)
              USE GLOBAL
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: IT
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              INTEGER(KIND=4) :: REGB(NBB)
              REAL(KIND=8) :: S
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: GB(NBB)
              REAL(KIND=8) :: QB(NBB)
            END SUBROUTINE SOLVE_DKE_QN_AMB
          END INTERFACE 
        END MODULE SOLVE_DKE_QN_AMB__genmod
