        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:13 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE ANA_INTEGRAL__genmod
          INTERFACE 
            SUBROUTINE ANA_INTEGRAL(IW,DZB,LAMBDA,BB,BPB,HBPPB,VDB,NQ,  &
     &QANA)
              INTEGER(KIND=4) :: NQ
              INTEGER(KIND=4) :: IW
              REAL(KIND=8) :: DZB
              REAL(KIND=8) :: LAMBDA
              REAL(KIND=8) :: BB
              REAL(KIND=8) :: BPB
              REAL(KIND=8) :: HBPPB
              REAL(KIND=8) :: VDB(3)
              REAL(KIND=8) :: QANA(NQ)
            END SUBROUTINE ANA_INTEGRAL
          END INTERFACE 
        END MODULE ANA_INTEGRAL__genmod
