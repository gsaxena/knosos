        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:45 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE READ_PLASMAS__genmod
          INTERFACE 
            SUBROUTINE READ_PLASMAS(NBB,FRACB,S0,ZB,AB,NB,DNBDPSI,TB,   &
     &DTBDPSI,EPSI)
              INTEGER(KIND=4) :: NBB
              REAL(KIND=8) :: FRACB(NBB)
              REAL(KIND=8) :: S0
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
            END SUBROUTINE READ_PLASMAS
          END INTERFACE 
        END MODULE READ_PLASMAS__genmod
