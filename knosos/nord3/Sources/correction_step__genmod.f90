        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:22 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CORRECTION_STEP__genmod
          INTERFACE 
            SUBROUTINE CORRECTION_STEP(SPLIT,IT,S,ALPHA,THETA,ZETA,     &
     &E_O_MU,ZL,TL,ZR,TR,DB,J,DJDS,DJDA,DSDT,DADT,DSDA,J0,DS,DA,DST,DAT,&
     &STEP_A,SMIN,SMAX,FD,ZETA_SPLIT,J_SPLIT,DJDS_SPLIT,DJDA_SPLIT)
              LOGICAL(KIND=4) :: SPLIT
              INTEGER(KIND=4) :: IT
              REAL(KIND=8) :: S
              REAL(KIND=8) :: ALPHA
              REAL(KIND=8) :: THETA
              REAL(KIND=8) :: ZETA
              REAL(KIND=8) :: E_O_MU
              REAL(KIND=8) :: ZL
              REAL(KIND=8) :: TL
              REAL(KIND=8) :: ZR
              REAL(KIND=8) :: TR
              REAL(KIND=8) :: DB
              REAL(KIND=8) :: J
              REAL(KIND=8) :: DJDS
              REAL(KIND=8) :: DJDA
              REAL(KIND=8) :: DSDT
              REAL(KIND=8) :: DADT
              REAL(KIND=8) :: DSDA
              REAL(KIND=8) :: J0
              REAL(KIND=8) :: DS
              REAL(KIND=8) :: DA
              REAL(KIND=8) :: DST
              REAL(KIND=8) :: DAT
              LOGICAL(KIND=4) :: STEP_A
              REAL(KIND=8) :: SMIN
              REAL(KIND=8) :: SMAX
              REAL(KIND=8) :: FD
              REAL(KIND=8) :: ZETA_SPLIT
              REAL(KIND=8) :: J_SPLIT
              REAL(KIND=8) :: DJDS_SPLIT
              REAL(KIND=8) :: DJDA_SPLIT
            END SUBROUTINE CORRECTION_STEP
          END INTERFACE 
        END MODULE CORRECTION_STEP__genmod
