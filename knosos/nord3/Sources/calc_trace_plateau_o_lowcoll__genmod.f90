        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:49 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_TRACE_PLATEAU_O_LOWCOLL__genmod
          INTERFACE 
            SUBROUTINE CALC_TRACE_PLATEAU_O_LOWCOLL(JT,IB,NBB,ZB,AB,S,NB&
     &,DNBDPSI,TB,DTBDPSI,EPSI,NALPHAB,B,PHI1,F_C,F_S,U0,MBB,TRM,GB)
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: NBB
              INTEGER(KIND=4) :: JT
              INTEGER(KIND=4) :: IB
              REAL(KIND=8) :: ZB(NBB)
              REAL(KIND=8) :: AB(NBB)
              REAL(KIND=8) :: S
              REAL(KIND=8) :: NB(NBB)
              REAL(KIND=8) :: DNBDPSI(NBB)
              REAL(KIND=8) :: TB(NBB)
              REAL(KIND=8) :: DTBDPSI(NBB)
              REAL(KIND=8) :: EPSI
              REAL(KIND=8) :: B(NALPHAB,NALPHAB)
              REAL(KIND=8) :: PHI1(NALPHAB,NALPHAB)
              REAL(KIND=8) :: F_C
              REAL(KIND=8) :: F_S
              REAL(KIND=8) :: U0(NALPHAB,NALPHAB)
              REAL(KIND=8) :: MBB(NALPHAB,NALPHAB)
              REAL(KIND=8) :: TRM(NALPHAB,NALPHAB)
              REAL(KIND=8) :: GB
            END SUBROUTINE CALC_TRACE_PLATEAU_O_LOWCOLL
          END INTERFACE 
        END MODULE CALC_TRACE_PLATEAU_O_LOWCOLL__genmod
