        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE PRECALC_TRIG__genmod
          INTERFACE 
            SUBROUTINE PRECALC_TRIG(NALPHAB,ZETA,THETA,TRIG,DTRIGDZ,    &
     &DTRIGDT)
              USE GLOBAL
              INTEGER(KIND=4) :: NALPHAB
              REAL(KIND=8) :: ZETA(NALPHAB)
              REAL(KIND=8) :: THETA(NALPHAB)
              REAL(KIND=8) :: TRIG(NNMP,NALPHAB*NALPHAB)
              REAL(KIND=8) :: DTRIGDZ(NNMP,NALPHAB*NALPHAB)
              REAL(KIND=8) :: DTRIGDT(NNMP,NALPHAB*NALPHAB)
            END SUBROUTINE PRECALC_TRIG
          END INTERFACE 
        END MODULE PRECALC_TRIG__genmod
