        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:11 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CALC_ALPHABETA__genmod
          INTERFACE 
            SUBROUTINE CALC_ALPHABETA(K,N,ALPHA_1NU,BETA_1NU,ALPHA_SBP, &
     &BETA_SBP)
              REAL(KIND=8) :: K
              INTEGER(KIND=4) :: N
              REAL(KIND=8) :: ALPHA_1NU
              REAL(KIND=8) :: BETA_1NU
              REAL(KIND=8) :: ALPHA_SBP
              REAL(KIND=8) :: BETA_SBP
            END SUBROUTINE CALC_ALPHABETA
          END INTERFACE 
        END MODULE CALC_ALPHABETA__genmod
