        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:05 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE READ_BULKSPECIES__genmod
          INTERFACE 
            SUBROUTINE READ_BULKSPECIES(NALPHAB,FILENAME,Q,FACT)
              INTEGER(KIND=4) :: NALPHAB
              CHARACTER(LEN=3) :: FILENAME
              REAL(KIND=8) :: Q(NALPHAB,NALPHAB)
              REAL(KIND=8) :: FACT
            END SUBROUTINE READ_BULKSPECIES
          END INTERFACE 
        END MODULE READ_BULKSPECIES__genmod
