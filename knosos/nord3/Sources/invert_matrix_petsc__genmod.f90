        !COMPILER-GENERATED INTERFACE MODULE: Mon Nov 14 12:10:30 2022
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INVERT_MATRIX_PETSC__genmod
          INTERFACE 
            SUBROUTINE INVERT_MATRIX_PETSC(NALPHAB,JV,NPOINT,MATA,BI3,  &
     &BI8,FACTNU,PHI1C,KSP,GINT)
              USE GLOBAL
              USE PETSCKSP
              INTEGER(KIND=4) :: NPOINT
              INTEGER(KIND=4) :: NALPHAB
              INTEGER(KIND=4) :: JV
              TYPE (TMAT) :: MATA
              REAL(KIND=8) :: BI3(NPOINT)
              REAL(KIND=8) :: BI8(NPOINT,NNMP)
              REAL(KIND=8) :: FACTNU(NPOINT)
              REAL(KIND=8) :: PHI1C(NNMP)
              TYPE (TKSP) :: KSP
              REAL(KIND=8) :: GINT(NPOINT,NNMP)
            END SUBROUTINE INVERT_MATRIX_PETSC
          END INTERFACE 
        END MODULE INVERT_MATRIX_PETSC__genmod
